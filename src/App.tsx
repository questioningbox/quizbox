import { Box, CssBaseline, ThemeProvider } from "@mui/material";
import React, { useEffect } from "react";
import themeConfiguration from "./configuration/theme.configuration";
import { Router } from "./router";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useAppDispatch, useAppSelector } from "./app/hooks";
import { isExpired } from "react-jwt";
import { clearResponse, responseFailed } from "./features/slice/ResponseSlice";
import { userLogout } from "./features/auth/UserSlice";
import { AlertModal } from "./components";
import { useTimer } from "use-timer";

export default function App() {
  const { error, message } = useAppSelector((state) => state.ResponseReducer);
  const { user } = useAppSelector((state) => state.UserReducer);
  const dispatch = useAppDispatch();
  const { time, start, pause } = useTimer({
    initialTime: 0,
    interval: 1000,
    onTimeUpdate: handleAuthorization,
  });

  function handleAuthorization() {
    if (user && user.token) {
      if (isExpired(user.token)) {
        dispatch(responseFailed("Session Expired, Please Login"));
        dispatch(userLogout());
      }
    } else {
      pause();
    }
  }

  useEffect(() => {
    start();
  }, []);
  const handleCloseNotifier = () => {
    dispatch(clearResponse());
  };

  function HandleNotifier() {
    Boolean(message) &&
      toast.success(message, {
        position: toast.POSITION.TOP_RIGHT,
        theme: "dark",
        onClose: handleCloseNotifier,
        toastId: "success",
      });

    Boolean(error) &&
      toast.error(error, {
        position: toast.POSITION.TOP_RIGHT,
        theme: "dark",
        onClose: handleCloseNotifier,
        toastId: "error",
      });

    return <Box></Box>;
  }
  return (
    <ThemeProvider theme={themeConfiguration}>
      <CssBaseline />
      <AlertModal />
      {/* {Boolean(error || message) && HandleNotifier()} */}
      {/* <ToastContainer draggable={true} autoClose={3000} /> */}
      <Router />
    </ThemeProvider>
  );
}
