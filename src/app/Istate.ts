import { UserProfileInfo } from "./../models/ProfileModel";
import { PagedResults } from "../models";
import CompetitionModel from "../models/CompetitionModel";
import ContestModel, { ContestQuestion } from "../models/ContestModel";
import { CourseInfo, CourseModel } from "../models/CourseModel";
import FeaturesModel from "../models/FeaturesModel";
import FeedModel from "../models/FeedModel";
import GroupModel from "../models/GroupModel";
import ProfileModel from "../models/ProfileModel";
import UserModel, { UserAnalytics } from "../models/UserModel";

export interface IAppReducerState {
  feed: boolean;
}

export interface IUserReducerState {
  user: UserModel | null;
  users: UserModel[];
  profile: ProfileModel | null;
  analytics: UserAnalytics;
  profiles: PagedResults<UserProfileInfo>;
}

export interface IResponseReducerState {
  loading: boolean;
  message: any;
  error: any;
}

export interface IFeedsReducerState {
  feeds: FeedModel[];
}

export interface ICoursesReducerState {
  courses: PagedResults<CourseInfo>;
}

export interface IFeaturesReducerState {
  features: FeaturesModel;
}

export interface IGroupsReducerState {
  groups: PagedResults<GroupModel>;
  group: GroupModel | null;
}

export interface IContestReducerState {
  contests: ContestModel[];
  contest: ContestModel | null;
  activeQuestion: ContestQuestion | null;
  errorMessage: any;
  message: any;
}

export interface ICompetitionReducerState {
  competitions: PagedResults<CompetitionModel>;
  loading: boolean;
  message: any;
  error: any;
}

export interface IStudentsReducerState {
  loading: boolean;
  error: any;
  message: any;
  students: PagedResults<UserProfileInfo>;
}

export interface ITeachersReducerState {
  loading: boolean;
  error: any;
  message: any;
  teachers: PagedResults<UserProfileInfo>;
}
