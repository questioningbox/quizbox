import { combineReducers } from "@reduxjs/toolkit";
import {
  AppReducer,
  ResponseReducer,
  FeedsReducer,
  CoursesReducer,
  FeaturesReducer,
  GroupsReducer,
  ContestReducer,
  CompetitionsReducer,
  TeachersReducer,
  StudentsReducer,
} from "../features/slice";
import { UserReducer } from "../features/auth";

////
export default combineReducers({
  AppReducer,
  ResponseReducer,
  UserReducer,
  FeedsReducer,
  CoursesReducer,
  GroupsReducer,
  FeaturesReducer,
  ContestReducer,
  CompetitionsReducer,
  TeachersReducer,
  StudentsReducer,
});
