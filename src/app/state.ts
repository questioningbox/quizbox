import {
  IAppReducerState,
  ICompetitionReducerState,
  IContestReducerState,
  ICoursesReducerState,
  IFeaturesReducerState,
  IFeedsReducerState,
  IGroupsReducerState,
  IResponseReducerState,
  IStudentsReducerState,
  ITeachersReducerState,
  IUserReducerState,
} from "./Istate";

export const AppReducerState: IAppReducerState = {
  feed: false,
};

export const UserReducerState: IUserReducerState = {
  user: null,
  users: [],
  profile: null,
  analytics: {
    competitions: 0,
    competitionsWon: 0,
    posts: 0,
    prepTest: 0,
    followers: 0,
    following: 0,
  },
  profiles: {
    results: [],
    page: 1,
    pageSize: 100,
    totalPages: 0,
    totalDocuments: 0,
  },
};

export const ResponseReducerState: IResponseReducerState = {
  loading: false,
  message: null,
  error: null,
};

export const FeedsReducerState: IFeedsReducerState = {
  feeds: [],
};

export const CoursesReducerState: ICoursesReducerState = {
  courses: {
    results: [],
    page: 1,
    pageSize: 10,
    totalDocuments: 0,
    totalPages: 0,
  },
};

export const FeaturesReducerState: IFeaturesReducerState = {
  features: {
    questionsCategories: [],
    educationLevels: [],
    usersCategory: [],
    billingRates: [],
    pricing: [],
    challengeCategories: [],
  },
};

export const GroupsReducerState: IGroupsReducerState = {
  group: null,
  groups: {
    results: [],
    page: 0,
    pageSize: 10,
    totalDocuments: 0,
    totalPages: 0,
  },
};

export const ContestReducerState: IContestReducerState = {
  contests: [],
  contest: null,
  activeQuestion: null,
  errorMessage: null,
  message: null,
};

export const CompetitionsReducerState: ICompetitionReducerState = {
  competitions: {
    page: 0,
    pageSize: 10,
    results: [],
    totalDocuments: 0,
    totalPages: 0,
  },
  loading: false,
  error: null,
  message: null,
};

export const StudentsReducerState: IStudentsReducerState = {
  loading: false,
  message: null,
  error: null,
  students: {
    page: 1,
    pageSize: 10,
    totalPages: 0,
    totalDocuments: 0,
    results: [],
  },
};

export const TeachersReducerState: ITeachersReducerState = {
  loading: false,
  message: null,
  error: null,
  teachers: {
    page: 1,
    pageSize: 10,
    totalDocuments: 0,
    totalPages: 0,
    results: [],
  },
};
