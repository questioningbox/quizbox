import { Box, CircularProgress, Stack } from "@mui/material";
import React from "react";

export default function ContentLoader() {
  return (
    <Stack
      height="100%"
      width="100%"
      alignItems="center"
      justifyContent="center"
      padding={10}
    >
      <Box sx={{ display: "flex" }}>
        <CircularProgress />
      </Box>
    </Stack>
  );
}
