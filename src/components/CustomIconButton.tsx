import {
  Chip,
  ChipProps,
  SvgIconProps,
  SvgIconTypeMap,
  Typography,
} from "@mui/material";
import { OverridableComponent } from "@mui/material/OverridableComponent";
import React from "react";
import { IconType } from "react-icons";
import { useAppSelector } from "../app/hooks";

interface IProps {
  Icon: OverridableComponent<SvgIconTypeMap<{}, "svg">> | IconType;
  title: string;
  handleClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
  props?: ChipProps;
  iconSize?: number;
  iconColor?: any;
  iconProps?: SvgIconProps;
}
export default function CustomIconButton({
  Icon,
  title,
  handleClick,
  props,
  iconColor,
  iconProps,
  iconSize,
}: IProps) {
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  return (
    <Chip
      size="medium"
      color="primary"
      sx={(theme) => ({
        bgcolor: theme.palette.primary.main,
        color: theme.palette.common.white,
        borderRadius: theme.spacing(0.5),
      })}
      disabled={loading}
      onClick={handleClick}
      avatar={
        <Icon
          style={{ background: "transparent" }}
          color={iconColor ? iconColor : "inherit"}
          {...iconProps}
          size={iconSize}
          fontSize="small"
        />
      }
      label={
        <Typography variant="caption">
          {loading ? "loading.." : title}
        </Typography>
      }
      {...props}
    />
  );
}
