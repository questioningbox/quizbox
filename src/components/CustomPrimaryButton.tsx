import { colors } from "@mui/material";
import IconButton, { IconButtonProps } from "@mui/material/IconButton";
import React, { MouseEvent } from "react";

interface IProps {
  title: string;
  handleClick?: (e: MouseEvent<HTMLButtonElement>) => void;
  variant: "contained" | "outlined";
  props?: IconButtonProps;
  fullWidth?: boolean;
}

export default function CustomPrimaryButton({
  title,
  handleClick,
  variant,
  props,
  fullWidth,
}: IProps) {
  return (
    <IconButton
      sx={(theme) => ({
        borderRadius: theme.spacing(0.45),
        width: fullWidth ? "100%" : title.length * 25,
        border: (theme) =>
          variant === "contained"
            ? "none"
            : `1px solid ${theme.palette.primary.main}`,
        bgcolor:
          variant === "contained" ? theme.palette.primary.main : "inherit",
        color: variant === "contained" ? theme.palette.common.white : "inherit",
        "&:hover": {
          bgcolor:
            variant === "contained"
              ? colors.blue[900]
              : theme.palette.action.hover,
        },
        height: "35px",
        fontSize: theme.spacing(2),
      })}
      onClick={handleClick}
      {...props}
    >
      {title}
    </IconButton>
  );
}
