import * as React from "react";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { MobileTimePicker } from "@mui/x-date-pickers/MobileTimePicker";
import dayjs, { Dayjs } from "dayjs";
import { TextField } from "@mui/material";

interface IProps {
  label?: string;
  handleChange: (value: any) => void;
}
export default function CustomTimePicker({ label, handleChange }: IProps) {
  const [value, setValue] = React.useState<Dayjs | null>(
    dayjs(dayjs().format("hh:mm:ss a"))
  );
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <MobileTimePicker
        label={label ? label : "Time"}
        views={["hours", "minutes"]}
        value={value}
        onChange={(e) => {
          setValue(e);
          handleChange(e);
        }}
        renderInput={(params) => (
          <TextField fullWidth variant="outlined" size="small" {...params} />
        )}
      />
      {/* <MobileTimePicker label={'"hours"'} views={["hours"]} />
        <MobileTimePicker
          label={'"minutes" and "seconds"'}
          views={["minutes", "seconds"]}
          format="mm:ss"
        /> */}
    </LocalizationProvider>
  );
}
