import { Stack, Typography } from "@mui/material";
import React from "react";
import { SlFeed } from "react-icons/sl";
export default function EmptyFeedsCard() {
  return (
    <Stack
      sx={(theme) => ({
        width: "100%",
        height: "100%",
        alignItems: "center",
        justifyContent: "center",
      })}
      spacing={1.5}
    >
      <SlFeed color="#c0c0c0" size={125} />
      <Typography variant="body1">No Feeds To Show</Typography>
    </Stack>
  );
}
