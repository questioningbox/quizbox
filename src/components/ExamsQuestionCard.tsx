import {
  Radio,
  RadioGroup,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { useState } from "react";
import { ContestQuestion } from "../models/ContestModel";

interface IProps {
  info: ContestQuestion;
  handleAnswer: (answers: string[]) => void;
  isTimeOver: boolean;
}
export default function ExamsQuestionCard({
  info,
  handleAnswer,
  isTimeOver,
}: IProps) {
  const [ans, setAns] = useState<string>("");
  const isMobile = useMediaQuery("(max-width:567px)");
  return (
    <Stack width="100%">
      <Stack
        padding={1.5}
        borderRadius={(theme) => theme.spacing(0)}
        border={(theme) => `1px solid ${theme.palette.action.hover}`}
        spacing={1}
      >
        <Typography variant="body1" fontWeight={500}>
          Qustion 1
        </Typography>
        <Typography variant="body2">{info.question.statement}</Typography>
      </Stack>
      <Stack spacing={1}>
        <RadioGroup
          onChange={(e) => {
            setAns(e.target.value);
            handleAnswer([e.target.value]);
          }}
          name="answer"
        >
          {info.question.options.map((option, i) => (
            <Stack
              alignItems="center"
              justifyContent="flex-start"
              sx={(theme) => ({
                padding: theme.spacing(0.25, 1),
                border: `1px solid ${theme.palette.action.disabledBackground}`,
                borderRadius: theme.spacing(0.5),
                flex: 1,
                [theme.breakpoints.down("sm")]: {
                  width: "100%",
                  margin: theme.spacing(0.25, 0),
                },
              })}
              my={1}
              direction="row"
            >
              <Radio
                disabled={isTimeOver}
                id={option}
                size="small"
                value={option}
              />
              <Typography
                sx={(theme) => ({
                  cursor: "pointer",
                })}
                component="label"
                htmlFor={option}
                variant="body2"
                color={ans === option ? "primary" : "default"}
              >
                {option}
              </Typography>
            </Stack>
          ))}
        </RadioGroup>
      </Stack>
    </Stack>
  );
}
