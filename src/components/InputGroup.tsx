import { Visibility, VisibilityOffOutlined } from "@mui/icons-material";
import {
  IconButton,
  InputAdornment,
  Stack,
  TextField,
  TextFieldProps,
  Typography,
} from "@mui/material";
import React, { ChangeEvent, ReactNode } from "react";

interface IProps {
  placeholder?: string;
  label?: string;
  handleChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  props?: TextFieldProps;
  children?: ReactNode;
}
export default function InputGroup({
  placeholder,
  label,
  handleChange,
  props,
  children,
}: IProps) {
  return (
    <Stack spacing={0.5}>
      {label && <Typography variant="body1">{label}</Typography>}
      <TextField
        size="small"
        fullWidth
        variant="outlined"
        color="primary"
        placeholder={placeholder}
        required
        autoComplete="off"
        onChange={handleChange}
        {...props}
      >
        {children}
      </TextField>
    </Stack>
  );
}
