import { Button, ButtonProps } from "@mui/material";
import React, { MouseEvent } from "react";
import { useAppSelector } from "../app/hooks";

interface IProps {
  title: string;
  handleClick: (e: MouseEvent<HTMLButtonElement>) => void;
  props?: ButtonProps;
}
export default function PrimaryButton({ handleClick, title, props }: IProps) {
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  return (
    <Button
      sx={(theme) => ({
        textTransform: "none",
      })}
      disableElevation
      variant="contained"
      size="medium"
      color="primary"
      onClick={handleClick}
      {...props}
      disabled={loading}
    >
      {loading ? "loading..." : title}
    </Button>
  );
}
