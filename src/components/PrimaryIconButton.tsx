import {
  IconButton,
  IconButtonProps,
  IconTypeMap,
  Typography,
} from "@mui/material";
import { OverridableComponent } from "@mui/material/OverridableComponent";
import React from "react";
import { IconType } from "react-icons/lib";

interface IProps {
  Icon: OverridableComponent<IconTypeMap<{}, "svg">> | IconType;
  title: string;
  props?: IconButtonProps;
  handleClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
}
export default function PrimaryIconButton({
  Icon,
  title,
  props,
  handleClick,
}: IProps) {
  return (
    <IconButton
      sx={(theme) => ({
        borderRadius: theme.spacing(0.45),
        bgcolor: theme.palette.primary.main,
        color: theme.palette.common.white,
        heigth: "30px",
        "&:hover": {
          bgcolor: theme.palette.primary.dark,
        },
      })}
      color="primary"
      onClick={handleClick}
      {...props}
    >
      <Typography variant="caption">{title}</Typography>
      <Icon fontSize="small" style={{ color: "#ffffff" }} />
    </IconButton>
  );
}
