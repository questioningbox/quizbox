import { Link, LinkProps } from "@mui/material";
import React from "react";

interface IProps {
  props?: LinkProps;
  route?: string;
  text: string;
}
export default function PrimaryLinkButton({ route, text, props }: IProps) {
  return (
    <Link
      sx={(theme) => ({
        textDecoration: "none",
        padding: theme.spacing(0.45, 1),
        bgcolor: theme.palette.primary.main,
        color: theme.palette.common.white,
        borderRadius: theme.spacing(0.45),
        "&:hover": {
          bgcolor: theme.palette.primary.dark,
        },
        transition: "all 0.45s ease-in-out",
      })}
      href={route}
      {...props}
    >
      {text}
    </Link>
  );
}
