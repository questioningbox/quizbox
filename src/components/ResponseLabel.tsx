import { useAppDispatch, useAppSelector } from "../app/hooks";

import React from "react";
import { IconButton, Stack, Typography } from "@mui/material";
import { IoMdClose } from "react-icons/io";
import { clearResponse } from "../features/slice/ResponseSlice";

export default function ResponseLabel() {
  const dispatch = useAppDispatch();
  const { error, message } = useAppSelector((state) => state.ResponseReducer);
  if (error || message) {
    return (
      <Stack
        alignItems="center"
        justifyContent="center"
        padding={(theme) => theme.spacing(0.25)}
        direction="row"
        spacing={2}
      >
        {error && (
          <Typography color="error" textAlign="center" variant="body1">
            {error}
          </Typography>
        )}
        {message && (
          <Typography
            variant="body1"
            textAlign="center"
            color="green"
            fontWeight={500}
          >
            {message}
          </Typography>
        )}
        <IconButton onClick={() => dispatch(clearResponse())} size="small">
          <IoMdClose />
        </IconButton>
      </Stack>
    );
  } else {
    return <React.Fragment />;
  }
}
