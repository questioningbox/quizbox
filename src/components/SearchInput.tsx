import { Stack } from "@mui/material";
import React, { ChangeEvent } from "react";
import { CiSearch } from "react-icons/ci";

interface IProps {
  placeholder?: string;
  handleChange?: (e: ChangeEvent<HTMLInputElement>) => void;
}
export default function SearchInput({ placeholder, handleChange }: IProps) {
  return (
    <Stack
      direction="row"
      alignItems="center"
      justifyContent="center"
      minWidth="200px"
      border={(theme) => `1px solid ${theme.palette.action.hover}`}
      spacing={0.5}
      padding={(theme) => theme.spacing(0.5, 1)}
      borderRadius={(theme) => theme.spacing(0.85)}
    >
      <CiSearch size={14} />
      <input
        placeholder={placeholder ? placeholder : "search.."}
        type="search"
        style={{
          outline: "none",
          borderStyle: "none",
          padding: "3px",
          flex: 1,
        }}
        onChange={handleChange}
      />
    </Stack>
  );
}
