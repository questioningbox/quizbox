import { IconButton, Stack, Typography } from "@mui/material";
import React from "react";
import { useLocation } from "react-router-dom";
import { AppColors } from "../constants";
import ISidebarLink from "../data/data";

interface IProps {
  info: ISidebarLink;
  handleClick?: () => void;
}
export default function SidebarLink({ info, handleClick }: IProps) {
  const path = useLocation();
  return (
    <Stack alignItems="center">
      <IconButton
        sx={(theme) => ({
          borderRadius: theme.spacing(1),
          borderColor: theme.palette.action.disabledBackground,
          borderWidth: 1,
          border: `1px solid ${theme.palette.action.hover}`,
        })}
        size="small"
        onClick={handleClick}
      >
        <info.Icon
          size={18}
          style={{
            color: info.paths.includes(path.pathname)
              ? AppColors.primary
              : "#c0c0c0",
          }}
        />
      </IconButton>
      <Typography
        fontSize={(theme) => theme.spacing(1.5)}
        variant="body2"
        textAlign="center"
        sx={(theme) => ({
          color: info.paths.includes(path.pathname)
            ? AppColors.primary
            : AppColors.darkbg,
        })}
      >
        {info.title}
      </Typography>
    </Stack>
  );
}
