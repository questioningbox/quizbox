import { Link, LinkProps } from "@mui/material";
import React from "react";

interface IProps {
  route?: string;
  text: string;
  props?: LinkProps;
}
export default function TextLink({ route, text, props }: IProps) {
  return (
    <Link
      sx={(theme) => ({
        fontSize: theme.spacing(1.45),
      })}
      href={route}
      {...props}
    >
      {text}
    </Link>
  );
}
