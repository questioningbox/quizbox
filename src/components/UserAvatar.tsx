import { Stack, StackProps } from "@mui/material";
import React from "react";

interface IProps {
  width?: number;
  height?: number;
  size?: "small" | "medium" | "large" | "xsmall";
  source: string;
  props?: StackProps;
}
export default function UserAvatar({
  width,
  height,
  size,
  source,
  props,
}: IProps) {
  if (size === "small") {
    return (
      <Stack
        sx={(theme) => ({
          width: width ? width : "50px",
          height: height ? height : "50px",
          borderRadius: "100%",
          bgcolor: theme.palette.action.hover,
          border: `1px solid ${theme.palette.action.disabledBackground}`,
          overflow: "hidden",
        })}
        {...props}
      >
        <img className="img" src={source} alt="profile-avatar-small" />
      </Stack>
    );
  }
  if (size === "medium") {
    return (
      <Stack
        sx={(theme) => ({
          width: width ? width : "80px",
          height: height ? height : "80px",
          borderRadius: "100%",
          bgcolor: theme.palette.action.hover,
          border: `1px solid ${theme.palette.action.disabledBackground}`,
          overflow: "hidden",
        })}
        {...props}
      >
        <img className="img" src={source} alt="profile-avatar-medium" />
      </Stack>
    );
  }

  if (size === "large") {
    return (
      <Stack
        sx={(theme) => ({
          width: width ? width : "120px",
          height: height ? height : "120px",
          borderRadius: "100%",
          bgcolor: theme.palette.action.hover,
          border: `1px solid ${theme.palette.action.disabledBackground}`,
          overflow: "hidden",
        })}
        {...props}
      >
        <img className="img" src={source} alt="profile-avatar-medium" />
      </Stack>
    );
  }

  return (
    <Stack
      sx={(theme) => ({
        width: width ? width : "25px",
        height: height ? height : "25px",
        borderRadius: "100%",
        bgcolor: theme.palette.action.hover,
        border: `1px solid ${theme.palette.action.disabledBackground}`,
        overflow: "hidden",
      })}
      {...props}
    >
      <img className="img" src={source} alt="profile-avatar-small" />
    </Stack>
  );
}
