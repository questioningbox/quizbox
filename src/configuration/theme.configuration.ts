import { AppColors } from "./../constants/index";
import { createTheme } from "@mui/material";

export default createTheme({
  palette: {
    primary: {
      main: AppColors.primary,
    },
    common: {
      black: AppColors.black,
    },
  },
});
