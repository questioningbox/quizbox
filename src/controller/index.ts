/* eslint-disable import/no-anonymous-default-export */
import Axios from "axios";

export interface IController {
  method: "put" | "post" | "delete" | "get" | "patch";
  isFile?: boolean;
  params?: any;
  data?: any;
  url: string;
  baseurl?: string;
  token?: string;
}

// export const baseUrl = "http://192.168.115.144:3300";
export const baseUrl: string = "https://coral-app-bpdib.ondigitalocean.app/";
// export const baseUrl: string = "https://quizbox-rans.herokuapp.com";

export default function <T>({
  method,
  isFile,
  params,
  data,
  url,
  baseurl,
  token,
}: IController) {
  return new Promise<T>(function (resolve, reject) {
    try {
      Axios({
        baseURL: baseUrl,
        method,
        data,
        url,
        params,
        headers: {
          authorization: `Bearer ${token}`,
          contentType: isFile ? "multipart/form-data" : "application/json",
        },
      })
        .then((response) => resolve(response.data as T))
        .catch((error) => {
          reject(error?.response?.data?.message || error?.message || error);
        });
    } catch (error) {
      reject(error);
    }
  });
}
