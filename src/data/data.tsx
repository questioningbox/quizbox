import { OverridableComponent } from "@mui/material/OverridableComponent";
import React from "react";
import { IconType } from "react-icons";
import {
  RiHome3Line,
  RiQuestionnaireLine,
  RiGroupLine,
  RiAccountPinCircleLine,
} from "react-icons/ri";
import { MdOutlineLiveHelp } from "react-icons/md";
import { FiCreditCard } from "react-icons/fi";
import NavigationRoutes from "../routes/NavigationRoutes";
import { SvgIconTypeMap } from "@mui/material";
import UserModel from "../models/UserModel";
import { HiOutlineUserGroup } from "react-icons/hi";

export default interface ISidebarLink {
  Icon: IconType | OverridableComponent<SvgIconTypeMap<{}, "svg">>;
  title: string;
  route?: string;
  paths: string[];
}

export const HomeSidebarLinks = (user: UserModel | null): ISidebarLink[] => {
  return user
    ? [
        { title: "Home", Icon: RiHome3Line, route: "/", paths: ["/"] },
        {
          title: "Prep Test",
          Icon: MdOutlineLiveHelp,
          route: NavigationRoutes.home.prepTest,
          paths: [
            NavigationRoutes.preptest.exams,
            NavigationRoutes.preptest.guide,
            NavigationRoutes.preptest.results,
            NavigationRoutes.preptest.root,
          ],
        },

        {
          title: "Questions",
          Icon: RiQuestionnaireLine,
          route: NavigationRoutes.home.questions,
          paths: [
            NavigationRoutes.questions.myquestions,
            NavigationRoutes.questions.root,
          ],
        },
        {
          title: "Groups",
          Icon: HiOutlineUserGroup,
          route: NavigationRoutes.groups.root,
          paths: [NavigationRoutes.groups.root, NavigationRoutes.groups.info],
        },
        {
          title: "Challenges",
          Icon: RiGroupLine,
          paths: [
            NavigationRoutes.challenge.all,
            NavigationRoutes.challenge.sent,
            NavigationRoutes.challenge.received,
            NavigationRoutes.challenge.one2many,
            NavigationRoutes.challenge.one2one,
            NavigationRoutes.challenge.leaderboard,
            NavigationRoutes.challenge.parcipants,
          ],
          route: NavigationRoutes.challenge.all,
        },
        {
          title: "Pricing",
          Icon: FiCreditCard,
          route: NavigationRoutes.pricing.root,
          paths: [NavigationRoutes.pricing.root],
        },
      ]
    : [
        { title: "Home", Icon: RiHome3Line, route: "/", paths: ["/"] },
        {
          title: "Prep Test",
          Icon: MdOutlineLiveHelp,
          route: NavigationRoutes.home.prepTest,
          paths: [
            NavigationRoutes.preptest.exams,
            NavigationRoutes.preptest.guide,
            NavigationRoutes.preptest.results,
            NavigationRoutes.preptest.root,
          ],
        },
        {
          title: "Questions",
          Icon: RiQuestionnaireLine,
          route: NavigationRoutes.home.questions,
          paths: [
            NavigationRoutes.questions.myquestions,
            NavigationRoutes.questions.root,
          ],
        },
        {
          title: "Challenges",
          Icon: RiGroupLine,
          paths: [
            NavigationRoutes.challenge.all,
            NavigationRoutes.challenge.sent,
            NavigationRoutes.challenge.received,
            NavigationRoutes.challenge.one2many,
            NavigationRoutes.challenge.one2one,
            NavigationRoutes.challenge.leaderboard,
            NavigationRoutes.challenge.parcipants,
          ],
          route: NavigationRoutes.challenge.all,
        },
        {
          title: "Pricing",
          Icon: FiCreditCard,
          route: NavigationRoutes.pricing.root,
          paths: [NavigationRoutes.pricing.root],
        },
        {
          title: "Sign In",
          Icon: RiAccountPinCircleLine,
          route: NavigationRoutes.account.login,
          paths: [
            NavigationRoutes.account.login,
            NavigationRoutes.account.register,
            NavigationRoutes.account.registerByPhone,
            NavigationRoutes.account.otpVerification,
            NavigationRoutes.account.resetPassword,
            NavigationRoutes.account.root,
            NavigationRoutes.account.forgotPassword,
          ],
        },
      ];
};
