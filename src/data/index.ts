import { IFooterLink } from "../interface";
import NavigationRoutes from "../routes/NavigationRoutes";

export const FooterLinks: IFooterLink[] = [
  { title: "Features", route: "" },
  { title: "Pricing", route: NavigationRoutes.pricing.root },
  { title: "Help", route: "" },
  { title: "Privacy", route: "" },
];
