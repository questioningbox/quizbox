import { ContestQuestion } from "./../../models/ContestModel";
import { createSlice } from "@reduxjs/toolkit";
import { ContestReducerState } from "../../app/state";
import ContestModel from "../../models/ContestModel";
import {
  ContestsThunk,
  ContestThunk,
  GetContestQuestionThunk,
} from "../../functions/slice";

const contestReducer = createSlice({
  name: "ContestReducer",
  initialState: ContestReducerState,
  reducers: {
    setContests: (state, action: { payload: ContestModel[] }) => {
      state.contests = action.payload;
    },
    setContest: (state, action: { payload: ContestModel | null }) => {
      state.contest = action.payload;
    },
    setActiveQuestion: (state, action: { payload: ContestQuestion | null }) => {
      state.activeQuestion = action.payload;
    },
    setErrorMessage: (state, action) => {
      state.errorMessage = action.payload;
      state.message = null;
    },
    setMessage: (state, action) => {
      state.message = action.payload;
      state.errorMessage = null;
    },
    clearResponseMessages: (state) => {
      state.errorMessage = null;
      state.message = null;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(GetContestQuestionThunk.fulfilled, (state, action) => {
        state.activeQuestion = action.payload.data.question;
        state.contest = action.payload.data.contest;
        state.message = action.payload.message;
        state.errorMessage = null;
      })
      .addCase(GetContestQuestionThunk.rejected, (state, action) => {
        state.errorMessage = action.error.message;
        state.message = null;
      })
      .addCase(ContestThunk.fulfilled, (state, action) => {
        state.contest = action.payload.data;
        state.message = action.payload.message;
        state.errorMessage = null;
      })
      .addCase(ContestThunk.rejected, (state, action) => {
        state.errorMessage = action.error.message;
        state.message = null;
      })
      .addCase(ContestsThunk.fulfilled, (state, action) => {
        state.contests = action.payload.data;
        state.message = action.payload.message;
        state.errorMessage = null;
      })
      .addCase(ContestsThunk.rejected, (state, action) => {
        state.errorMessage = action.error.message;
        state.message = null;
      });
  },
});

export default contestReducer.reducer;
export const {
  setContest,
  setActiveQuestion,
  setContests,
  setErrorMessage,
  setMessage,
  clearResponseMessages,
} = contestReducer.actions;
