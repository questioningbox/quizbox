import { createSlice } from "@reduxjs/toolkit";
import { CoursesReducerState } from "../../app/state";
import { CourseThunk } from "../../functions/slice";

const CourseReducer = createSlice({
  name: "CoursesReducer",
  initialState: CoursesReducerState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(CourseThunk.fulfilled, (state, action) => {
      state.courses = action.payload.data;
    });
  },
});

export default CourseReducer.reducer;

export const {} = CourseReducer.actions;
