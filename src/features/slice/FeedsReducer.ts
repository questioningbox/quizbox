import { createSlice } from "@reduxjs/toolkit";
import { FeedsReducerState } from "../../app/state";
import { FeedsThunk } from "../../functions/slice";
import FeedModel from "../../models/FeedModel";

const FeedsReducer = createSlice({
  name: "FeedsReducer",
  initialState: FeedsReducerState,
  reducers: {
    setFeeds: (state, action: { payload: FeedModel[] }) => {
      state.feeds = action.payload;
    },
  },
  extraReducers:builder=>{
    builder.addCase(FeedsThunk.fulfilled,(state,action)=>{
        state.feeds=action.payload.data
    })
  }
});

export default FeedsReducer.reducer;

export const { setFeeds } = FeedsReducer.actions;
