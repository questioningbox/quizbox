import { PagedResults } from "./../../models/index";
import { createSlice } from "@reduxjs/toolkit";
import { TeachersReducerState } from "../../app/state";
import { UserProfileInfo } from "../../models/ProfileModel";
import { TeachersThunk } from "../../functions/slice";

const teachersReducer = createSlice({
  name: "StudentsReducer",
  initialState: TeachersReducerState,
  reducers: {
    setTeachers: (
      state,
      action: { payload: PagedResults<UserProfileInfo> }
    ) => {
      state.teachers = action.payload;
    },
    setTeacherssLoading: (state) => {
      state.loading = true;
      state.error = null;
      state.message = null;
    },
    setTeachersError: (state, action) => {
      state.error = action.payload;
      state.message = null;
      state.loading = false;
    },
    setTeachersMessage: (state, action) => {
      state.error = null;
      state.message = action.payload;
      state.loading = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(TeachersThunk.pending, (state) => {
        state.error = null;
        state.message = null;
        state.loading = true;
      })
      .addCase(TeachersThunk.fulfilled, (state, action) => {
        state.error = null;
        state.message = action.payload.message;
        state.loading = false;
        state.teachers = action.payload.data;
      })
      .addCase(TeachersThunk.rejected, (state, action) => {
        state.error = action.error.message || action.error || action.payload;
        state.loading = false;
        state.message = null;
      });
  },
});

export const {
  setTeachers,
  setTeachersMessage,
  setTeachersError,
  setTeacherssLoading,
} = teachersReducer.actions;
export default teachersReducer.reducer;
