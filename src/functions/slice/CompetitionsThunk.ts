import controller, { IController } from "./../../controller/index";
import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiResponseModel from "../../models/ApiResponseModel";
import { PagedResults } from "../../models";
import CompetitionModel from "../../models/CompetitionModel";

export default createAsyncThunk(
  "api/competitions",
  async ({ data, method, url, token, params }: IController) => {
    try {
      return await controller<ApiResponseModel<PagedResults<CompetitionModel>>>(
        { data, method, url, token, params }
      );
    } catch (error) {
      throw error;
    }
  }
);
