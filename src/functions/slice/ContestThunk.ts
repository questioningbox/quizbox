import controller, { IController } from "./../../controller/index";
import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiResponseModel from "../../models/ApiResponseModel";
import ContestModel from "../../models/ContestModel";

export default createAsyncThunk(
  "api/contest",
  async ({ data, method, url, token, params }: IController) => {
    try {
      return await controller<ApiResponseModel<ContestModel>>({
        data,
        method,
        url,
        token,
        params,
      });
    } catch (error) {
      throw error;
    }
  }
);
