import controller, { IController } from "./../../controller/index";
import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiResponseModel from "../../models/ApiResponseModel";
import { CourseInfo, CourseModel } from "../../models/CourseModel";
import { PagedResults } from "../../models";

export default createAsyncThunk(
  "api/course",
  async ({ data, method, url, token, isFile }: IController) => {
    try {
      return await controller<ApiResponseModel<PagedResults<CourseInfo>>>({
        data,
        method,
        url,
        token,
        isFile,
      });
    } catch (error) {
      throw error;
    }
  }
);
