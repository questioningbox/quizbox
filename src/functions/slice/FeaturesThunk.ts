import controller, { IController } from "./../../controller/index";
import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiResponseModel from "../../models/ApiResponseModel";
import FeaturesModel from "../../models/FeaturesModel";

export default createAsyncThunk(
  "api/features/thunk",
  async ({ data, token, url, method, params }: IController) => {
    try {
      return await controller<ApiResponseModel<FeaturesModel>>({
        data,
        method,
        url,
        token,
        params,
      });
    } catch (error) {
      throw error;
    }
  }
);
