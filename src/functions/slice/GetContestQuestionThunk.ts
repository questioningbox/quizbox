import controller, { IController } from "../../controller/index";
import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiResponseModel from "../../models/ApiResponseModel";
import ContestModel, { ContestQuestion } from "../../models/ContestModel";

export default createAsyncThunk(
  "api/contest/question",
  async ({ data, url, token, method, params }: IController) => {
    try {
      return await controller<
        ApiResponseModel<{ contest: ContestModel; question: ContestQuestion }>
      >({
        data,
        method,
        url,
        token,
        params,
      });
    } catch (error) {
      throw error;
    }
  }
);
