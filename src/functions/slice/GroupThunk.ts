import controller, { IController } from "./../../controller/index";
import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiResponseModel from "../../models/ApiResponseModel";
import GroupModel from "../../models/GroupModel";

export default createAsyncThunk(
  "api/group/thunk",
  async ({ data, method, url, token, params }: IController) => {
    try {
      return await controller<ApiResponseModel<GroupModel>>({
        data,
        url,
        token,
        method,
        params,
      });
    } catch (error) {
      throw error;
    }
  }
);
