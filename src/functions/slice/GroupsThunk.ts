import controller, { IController } from "./../../controller/index";
import { createAsyncThunk } from "@reduxjs/toolkit";
import GroupModel from "../../models/GroupModel";
import ApiResponseModel from "../../models/ApiResponseModel";
import { PagedResults } from "../../models";

export default createAsyncThunk(
  "api/groups/thunk",
  async ({ data, method, url, token, params }: IController) => {
    try {
      return await controller<ApiResponseModel<PagedResults<GroupModel>>>({
        data,
        method,
        url,
        token,
        params,
      });
    } catch (error) {
      throw error;
    }
  }
);
