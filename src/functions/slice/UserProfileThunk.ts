import { createAsyncThunk } from "@reduxjs/toolkit";
import controller, { IController } from "../../controller";
import ApiResponseModel from "../../models/ApiResponseModel";
import ProfileModel from "../../models/ProfileModel";

export default createAsyncThunk(
  "api/user/profile",
  async ({ url, token, method, data }: IController) => {
    try {
      return await controller<ApiResponseModel<ProfileModel>>({
        url,
        token,
        data,
        method,
      });
    } catch (error) {
      throw error;
    }
  }
);
