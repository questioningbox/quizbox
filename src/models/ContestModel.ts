import QuestionsModel from "./QuestionsModel";

export default interface ContestModel {
  _id: string;
  contestId: string;
  status: string;
  userId: string;
  answers: ContestAnswer[];
  category: string;
  createdAt: string;
  updatedAt: string;
  questions: string[];

  solved: string[];
}

export interface ContestAnswer {
  selection: string[];
  score: number;
  answer: string[];
  mark: number;
  questionId: string;
}

export interface ContestQuestion {
  question: QuestionsModel;
  duration: {
    start: string;
    end: string;
  };
}
