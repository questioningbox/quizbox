import { FileInfoModel } from ".";
import { CreateGroupDto } from "../dto/CreateGroupDto";
import UserModel from "./UserModel";

export default interface GroupModel {
  _id: string;
  groupId: string;
  title: string;
  description: string;
  status: string;
  createdAt: string;
  updatedAt: string;
  createdBy: string;
  members: string[];
  admins: string[];
  tag: string;
  invites: string[];
  gallery: FileInfoModel[];
  banner?: FileInfoModel;
}

export interface GroupInfoModel extends GroupModel {
  creator: UserModel;
}
