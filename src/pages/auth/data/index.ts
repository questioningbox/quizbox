export interface IUserAccountType {
  title: string;
  value: string;
}

export const UserAccountTypes: IUserAccountType[] = [
  { title: "Student", value: "student" },
  { title: "Teacher", value: "teacher" },
  { title: "Institution", value: "institution" },
  { title: "Others", value: "others" },
];
