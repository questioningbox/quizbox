import { Stack, Typography, Button } from "@mui/material";
import React, { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { AppColors } from "../../../constants";
import FeaturesThunk from "../../../functions/slice/FeaturesThunk";
import ApiRoutes from "../../../routes/ApiRoutes";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { DummyUsersContainer } from "../../../views";
import Footer from "../../../views/Footer";

export default function AuthPage() {
  const navigation = useNavigate();
  const { user } = useAppSelector((state) => state.UserReducer);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (user) {
      !Boolean(user && user.authenticated) &&
        navigation(NavigationRoutes.account.otpVerification);
      Boolean(user && user.authenticated && user.name.length > 0) &&
        navigation(NavigationRoutes.home.root);
      Boolean(user && !user.authenticated && user.name.length <= 0) &&
        navigation(NavigationRoutes.account.resetPassword);
    }
  }, [user]);

  useEffect(() => {
    dispatch(FeaturesThunk({ method: "get", url: ApiRoutes.category.get }));
  }, []);

  return (
    <Stack
      width="100%"
      height="100vh"
      padding="0"
      margin="0"
      direction="row"
      alignItems="center"
      justifyContent="flex-start"
    >
      <Stack
        alignItems="center"
        height="100%"
        justifyContent="flex-start"
        flex={1}
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="flex-start"
          width="100%"
          alignSelf="flex-start"
          padding={2}
        >
          <Typography
            onClick={() => navigation("/")}
            fontWeight={900}
            variant="h5"
            sx={(theme) => ({
              cursor: "pointer",
            })}
          >
            QuizBox
          </Typography>
        </Stack>
        <Stack height="100%" width="100%">
          <Outlet />
        </Stack>
        <Stack flex={1} />
        <Footer />
      </Stack>
      <Stack
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            display: "none",
          },
          bgcolor: AppColors.darkbg,
          height: "100%",
        })}
        flex={1}
      >
        <Stack
          sx={(theme) => ({
            alignItems: "center",
            justifyContent: "center",
            height: "100%",
          })}
        >
          <Stack
            alignItems="center"
            justifyContent="center"
            height="100%"
            sx={(theme) => ({
              color: theme.palette.common.white,
              [theme.breakpoints.down("sm")]: {
                width: "100%",
                padding: theme.spacing(2),
              },
              width: "70%",
            })}
          >
            <Typography
              sx={(theme) => ({
                fontSize: theme.spacing(4),
                lineHeight: "40px",
              })}
              textAlign="center"
              variant="body1"
            >
              A single platform for education and Online Games
            </Typography>
            <Stack marginY={1} />
            <Typography textAlign="center" variant="body2">
              create and account and get full access to all features. Learn and
              play online games on the platform
            </Typography>
            <Stack marginY={1} />
            <DummyUsersContainer />
            <Stack marginTop={3} />
          </Stack>
        </Stack>
      </Stack>
    </Stack>
  );
}
