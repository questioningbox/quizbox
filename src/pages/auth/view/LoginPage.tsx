import { Visibility, VisibilityOffOutlined } from "@mui/icons-material";
import {
  Stack,
  Typography,
  TextField,
  InputAdornment,
  IconButton,
  Button,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { BiEnvelope } from "react-icons/bi";
import { CiUser } from "react-icons/ci";
import { FiLock, FiPhone } from "react-icons/fi";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { InputGroup, PrimaryButton, ResponseLabel } from "../../../components";
import { ILoginDto, ILoginWithEmailDto } from "../../../dto/Auth";
import { ValidateLoginInfo } from "../../../features/auth/Validation";
import { AuthThunk } from "../../../functions/auth";
import ApiRoutes from "../../../routes/ApiRoutes";
import NavigationRoutes from "../../../routes/NavigationRoutes";
export default function LoginPage() {
  const { user } = useAppSelector((state) => state.UserReducer);
  const [accountType, setAccountType] = useState<boolean>(false);
  const [loginInfo, setLoginInfo] = useState<ILoginDto>({
    username: "",
    password: "",
  });
  const { error } = useAppSelector((state) => state.ResponseReducer);
  const [validationError, setValidationError] = useState<{
    name: string;
    error: string;
  }>({ error: "", name: "" });
  const [showPassword, setShowPassword] = useState<Boolean>(false);
  const navigation = useNavigate();
  const dispatch = useAppDispatch();
  function handlePasswordVisibility() {
    setShowPassword(!showPassword);
  }

  function handleLogin() {
    try {
      ValidateLoginInfo(loginInfo);
      dispatch(
        AuthThunk({
          data: loginInfo,
          url: ApiRoutes.auth.login,
          method: "post",
        })
      );
      setValidationError({ name: "", error: "" });
    } catch (error) {
      setValidationError(error as any);
    }
  }

  return (
    <Stack
      height="100%"
      width="100%"
      alignItems="center"
      justifyContent="center"
      sx={(theme) => ({
        [theme.breakpoints.down("sm")]: {
          padding: theme.spacing(2),
        },
        padding: theme.spacing(4),
        overflowY: "auto",
      })}
      spacing={2}
    >
      <Typography fontWeight={800} textAlign="center" variant="h4">
        Welcome Back, Log In
      </Typography>
      <Stack
        spacing={1.75}
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            width: "100%",
          },
          width: "90%",
        })}
      >
        <InputGroup
          label="PhoneNumber/Email*"
          placeholder="PhoneNumber/Email"
          props={{
            InputProps: {
              startAdornment: (
                <InputAdornment position="start">
                  <CiUser />
                </InputAdornment>
              ),
            },
            type: "text",
            name: "username",
            error: validationError.name === "username",
          }}
          handleChange={(e) =>
            setLoginInfo({ ...loginInfo, username: e.target.value })
          }
        />
        {validationError.name === "username" && (
          <Typography variant="caption" textAlign="center" color="error">
            {validationError.error}
          </Typography>
        )}
        <InputGroup
          props={{
            InputProps: {
              startAdornment: (
                <InputAdornment position="start">
                  <FiLock />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton onClick={handlePasswordVisibility}>
                    {showPassword ? <Visibility /> : <VisibilityOffOutlined />}
                  </IconButton>
                </InputAdornment>
              ),
              type: showPassword ? "text" : "password",
              name: "password",
              error: validationError.name === "password",
            },
          }}
          label="Password*"
          placeholder="password"
          handleChange={(e) =>
            setLoginInfo({ ...loginInfo, password: e.target.value })
          }
        />
        {validationError.name === "password" && (
          <Typography variant="caption" textAlign="center" color="error">
            {validationError.error}
          </Typography>
        )}
        <PrimaryButton handleClick={handleLogin} title="Login" />

        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          width="100%"
        >
          <Button
            sx={(theme) => ({
              textTransform: "none",
              fontSize: theme.spacing(2),
            })}
            variant="text"
            color="primary"
            onClick={() => navigation(NavigationRoutes.account.register)}
          >
            don't have Account?
          </Button>
          <Stack flex={1} />
          <Button
            variant="text"
            size="small"
            sx={(theme) => ({
              textTransform: "none",
              color: theme.palette.common.black,
              fontSize: theme.spacing(2),
            })}
            onClick={() => navigation(NavigationRoutes.account.forgotPassword)}
          >
            Forgot Password
          </Button>
        </Stack>
        {/* <ResponseLabel /> */}
      </Stack>
    </Stack>
  );
}
