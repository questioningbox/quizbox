import { Visibility, VisibilityOffOutlined } from "@mui/icons-material";
import {
  Stack,
  Typography,
  TextField,
  InputAdornment,
  IconButton,
  MenuItem,
  Chip,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { FiLock, FiPhone } from "react-icons/fi";
import { BsArrowLeftShort } from "react-icons/bs";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { useNavigate } from "react-router-dom";
import { AiOutlineUser } from "react-icons/ai";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import {
  IValidationError,
  ValidateRegisterViaPhoneDto,
} from "../../../features/auth/Validation";
import { RegisterViaPhoneDto } from "../../../dto/Auth";
import { AuthThunk } from "../../../functions/auth";
import ApiRoutes from "../../../routes/ApiRoutes";
import { UserAccountTypes } from "../data";
import { InputGroup, PrimaryButton } from "../../../components";
import FeaturesThunk from "../../../functions/slice/FeaturesThunk";
import { CustomOutlinedCountryPicker } from "../../../shared";
///
export default function RegisterByPhonePage() {
  const [showPassword, setShowPassword] = useState<Boolean>(false);
  const { features } = useAppSelector((state) => state.FeaturesReducer);
  const [validationError, setValidationError] = useState<IValidationError>({
    name: "",
    error: null,
  });
  const dispatch = useAppDispatch();
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const [registerInfo, setRegisterInfo] = useState<RegisterViaPhoneDto>({
    userType: "student",
    phoneNumber: "",
    password: "",
    name: "",
    country: null,
  });
  const navigation = useNavigate();
  function handlePasswordVisibility() {
    setShowPassword(!showPassword);
  }

  function handleRegister() {
    try {
      ValidateRegisterViaPhoneDto(registerInfo);
      dispatch(
        AuthThunk({
          data: registerInfo,
          method: "post",
          url: ApiRoutes.auth.register.phone,
        })
      );
    } catch (error) {
      setValidationError(error as IValidationError);
    }
  }

  return (
    <Stack
      height="100%"
      width="100%"
      alignItems="center"
      justifyContent="center"
      sx={(theme) => ({
        [theme.breakpoints.down("sm")]: {
          padding: theme.spacing(2),
        },
        padding: theme.spacing(4),
      })}
      spacing={2}
    >
      <Typography fontWeight={900} variant="h5">
        Sign Up with Phone Number
      </Typography>
      <Stack
        spacing={1.75}
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            width: "100%",
          },
          width: "80%",
        })}
      >
        <InputGroup
          label="Name*"
          placeholder="enter full name"
          props={{
            InputProps: {
              startAdornment: (
                <InputAdornment position="start">
                  <AiOutlineUser />
                </InputAdornment>
              ),
            },
            name: "name",
            error: validationError.name === "name",
            type: "text",
            value: registerInfo.name,
          }}
          handleChange={(e) =>
            setRegisterInfo({ ...registerInfo, name: e.target.value })
          }
        />
        {validationError.name === "name" && (
          <Typography variant="caption" color="error" textAlign="center">
            {validationError.error}
          </Typography>
        )}
        <InputGroup
          label="PhoneNumber*"
          placeholder="enter phone number"
          props={{
            InputProps: {
              startAdornment: (
                <InputAdornment position="start">
                  <FiPhone />
                </InputAdornment>
              ),
            },
            type: "text",
            error: validationError.name === "phone",
            name: "phone",
            value: registerInfo.phoneNumber,
          }}
          handleChange={(e) =>
            setRegisterInfo({ ...registerInfo, phoneNumber: e.target.value })
          }
        />
        {validationError.name === "phone" && (
          <Typography variant="caption" color="error" textAlign="center">
            {validationError.error}
          </Typography>
        )}
        <InputGroup
          label="Password*"
          placeholder="enter password"
          props={{
            InputProps: {
              startAdornment: (
                <InputAdornment position="start">
                  <FiLock />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton onClick={handlePasswordVisibility}>
                    {showPassword ? <Visibility /> : <VisibilityOffOutlined />}
                  </IconButton>
                </InputAdornment>
              ),
            },
            type: showPassword ? "text" : "password",
            name: "password",
            error: validationError.name === "password",
            value: registerInfo.password,
          }}
          handleChange={(e) =>
            setRegisterInfo({ ...registerInfo, password: e.target.value })
          }
        />
        {validationError.name === "password" && (
          <Typography variant="caption" color="error" textAlign="center">
            {validationError.error}
          </Typography>
        )}

        <Stack spacing={1}>
          <Typography variant="body1">User Type</Typography>
          <TextField
            size="small"
            autoComplete="off"
            fullWidth
            variant="outlined"
            color="primary"
            label="User Type"
            select
          >
            {features.usersCategory.map((cat) => (
              <MenuItem key={cat._id} value={cat.title}>
                {cat.title}
              </MenuItem>
            ))}
          </TextField>
        </Stack>
        <CustomOutlinedCountryPicker
          handleChange={(e) => setRegisterInfo({ ...registerInfo, country: e })}
        />

        <PrimaryButton
          props={{ disabled: loading || !registerInfo.country }}
          title="Sign Up"
          handleClick={handleRegister}
        />

        <Stack
          direction="row"
          marginY={3}
          alignItems="center"
          justifyContent="center"
          width="100%"
        >
          <Chip
            sx={(theme) => ({
              bgcolor: theme.palette.common.white,
              borderRadius: theme.spacing(0),
              borderWidth: 0,
            })}
            onClick={() => navigation(NavigationRoutes.account.login)}
            size="small"
            avatar={<BsArrowLeftShort />}
            label={
              <Typography color="primary" variant="body2">
                Back to login
              </Typography>
            }
          />
        </Stack>
      </Stack>
    </Stack>
  );
}
