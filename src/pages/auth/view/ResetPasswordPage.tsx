import { Visibility, VisibilityOffOutlined } from "@mui/icons-material";
import {
  Stack,
  Typography,
  TextField,
  InputAdornment,
  IconButton,
  Button,
  Chip,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { FiLock } from "react-icons/fi";
import { useNavigate } from "react-router-dom";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { BsArrowLeftShort } from "react-icons/bs";
import { InputGroup, PrimaryButton, ResponseLabel } from "../../../components";
import { IResetPasswordDto } from "../../../dto/Auth";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { responseFailed } from "../../../features/slice/ResponseSlice";
import { ValidateResetPasswordInfo } from "../../../features/auth/Validation";
import { AuthThunk } from "../../../functions/auth";
import ApiRoutes from "../../../routes/ApiRoutes";

///
export default function ResetPasswordPage() {
  const [showPassword, setShowPassword] = useState<Boolean>(false);
  const { user } = useAppSelector((state) => state.UserReducer);
  const dispatch = useAppDispatch();
  const [info, setInfo] = useState<IResetPasswordDto>({
    code: "",
    password: "",
    username: "",
    confirmPassword: "",
  });
  const navigation = useNavigate();
  function handlePasswordVisibility() {
    setShowPassword(!showPassword);
  }

  useEffect(() => {
    setInfo({ ...info, username: user ? user.username : "" });
  }, []);

  function handlePasswordReset() {
    try {
      ValidateResetPasswordInfo(info);
      dispatch(
        AuthThunk({
          data: {
            code: info.code,
            password: info.password,
            username: info.username || (user && user.username) || "",
          },
          method: "post",
          url: ApiRoutes.auth.changePassword,
          token: user ? user.token : "",
        })
      );
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  return (
    <Stack
      height="100%"
      width="100%"
      alignItems="center"
      justifyContent="center"
      sx={(theme) => ({
        [theme.breakpoints.down("sm")]: {
          padding: theme.spacing(2),
        },
        padding: theme.spacing(4),
      })}
      spacing={2}
    >
      <Typography fontWeight={900} variant="h4">
        Reset Password
      </Typography>

      <Stack
        spacing={1.75}
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            width: "100%",
          },
          width: "80%",
        })}
      >
        <InputGroup
          props={{
            InputProps: {
              startAdornment: (
                <InputAdornment position="start">
                  <FiLock />
                </InputAdornment>
              ),
            },
            name: "otp",
            type: "number",
          }}
          label="Verification Code"
          placeholder="Enter OTP"
          handleChange={(e) => setInfo({ ...info, code: e.target.value })}
        />
        <InputGroup
          label="New Password*"
          props={{
            InputProps: {
              startAdornment: (
                <InputAdornment position="start">
                  <FiLock />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton onClick={handlePasswordVisibility}>
                    {showPassword ? <Visibility /> : <VisibilityOffOutlined />}
                  </IconButton>
                </InputAdornment>
              ),
            },
            type: showPassword ? "text" : "password",
            name: "password",
            placeholder: "Enter New Password",
          }}
          handleChange={(e) => setInfo({ ...info, password: e.target.value })}
        />
        <InputGroup
          label="Confirm Password*"
          props={{
            InputProps: {
              startAdornment: (
                <InputAdornment position="start">
                  <FiLock />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton onClick={handlePasswordVisibility}>
                    {showPassword ? <Visibility /> : <VisibilityOffOutlined />}
                  </IconButton>
                </InputAdornment>
              ),
            },
            type: showPassword ? "text" : "password",
            name: "password",
            placeholder: "Confirm Password",
          }}
          handleChange={(e) =>
            setInfo({ ...info, confirmPassword: e.target.value })
          }
        />
        <PrimaryButton
          title="Reset Password"
          handleClick={handlePasswordReset}
        />

        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          width="100%"
        >
          <Chip
            sx={(theme) => ({
              bgcolor: theme.palette.common.white,
              borderRadius: theme.spacing(0),
              borderWidth: 0,
            })}
            onClick={() => navigation(NavigationRoutes.account.login)}
            size="small"
            avatar={<BsArrowLeftShort />}
            label={
              <Typography color="primary" variant="body2">
                Back to login
              </Typography>
            }
          />
        </Stack>
        {/* <ResponseLabel /> */}
      </Stack>
    </Stack>
  );
}
