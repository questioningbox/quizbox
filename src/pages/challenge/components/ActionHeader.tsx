import { Button, Stack } from "@mui/material";
import React from "react";
import { CiTrophy } from "react-icons/ci";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppSelector } from "../../../app/hooks";
import { PrimaryLinkButton } from "../../../components";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { CustomIconButton } from "../../../shared";

interface IProps {
  handeAddNewChallenge: () => void;
}
export default function ActionHeader({ handeAddNewChallenge }: IProps) {
  const location = useLocation();
  const navigation = useNavigate();
  const { user } = useAppSelector((state) => state.UserReducer);
  return (
    <Stack
      direction="row"
      alignItems="center"
      justifyContent="space-between"
      width="100%"
      padding={(theme) => theme.spacing(1.5, 0)}
    >
      <Stack
        spacing={1}
        direction="row"
        alignItems="center"
        justifyContent="flex-start"
      >
        {/* <Button
          size="small"
          variant="text"
          onClick={() => navigation(NavigationRoutes.challenge.received)}
          sx={(theme) => ({
            textTransform: "none",
            color:
              location.pathname === NavigationRoutes.challenge.received
                ? theme.palette.primary.main
                : "inherit",
            borderBottom:
              location.pathname === NavigationRoutes.challenge.received
                ? `1px solid ${theme.palette.primary.main}`
                : "none",
            borderRadius: 0,
          })}
        >
          Received
        </Button>
        <Button
          size="small"
          variant="text"
          onClick={() => navigation(NavigationRoutes.challenge.sent)}
          sx={(theme) => ({
            textTransform: "none",
            color:
              location.pathname === NavigationRoutes.challenge.sent
                ? theme.palette.primary.main
                : "inherit",
            borderBottom:
              location.pathname === NavigationRoutes.challenge.sent
                ? `1px solid ${theme.palette.primary.main}`
                : "none",
            borderRadius: 0,
          })}
        >
          Sent
        </Button> */}
      </Stack>
      {user && (
        <CustomIconButton
          handleClick={handeAddNewChallenge}
          title="New Challenge"
          variant="contained"
          Icon={CiTrophy}
        />
      )}
    </Stack>
  );
}
