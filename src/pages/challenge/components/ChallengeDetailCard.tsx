import { Grid, Stack, Typography } from "@mui/material";
import React, { ReactNode } from "react";

interface IProps {
  title: string;
  subtitle: string;
  Icon: ReactNode;
}
export default function ChallengeDetailCard({ title, subtitle, Icon }: IProps) {
  return (
    <Grid item>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="center"
        padding={1}
        spacing={1}
      >
        {Icon}
        <Stack paddingX={1.5}>
          <Typography variant="body2">{title}</Typography>
          <Typography variant="caption">{subtitle}</Typography>
        </Stack>
      </Stack>
    </Grid>
  );
}
