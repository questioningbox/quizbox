import { Stack, Typography } from "@mui/material";
import React from "react";
import resources from "../../../resources";

export default function ChallengeHeaderTag() {
  return (
    <Stack
      direction="row"
      alignItems="center"
      justifyContent="flex-start"
      width="100%"
      padding={1}
      spacing={1}
      alignSelf="flex-start"
      paddingY={2}
    >
      <Typography variant="h5" fontWeight="bold">
        Leaderboard
      </Typography>
      <Stack width="25px" height="25px" overflow="hidden">
        <img src={resources.trophy} alt="challenge-page-tag-avatar" />
      </Stack>
    </Stack>
  );
}
