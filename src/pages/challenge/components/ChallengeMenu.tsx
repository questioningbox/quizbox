import { Menu, MenuItem } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import NavigationRoutes from "../../../routes/NavigationRoutes";

interface IProps {
  anchorEl: HTMLElement | null;
  handleClose: () => void;
}
export default function ChallengeMenu({ handleClose, anchorEl }: IProps) {
  const navigation = useNavigate();
  return (
    <Menu open={Boolean(anchorEl)} onClose={handleClose} anchorEl={anchorEl}>
      <MenuItem
        onClick={() => {
          navigation(NavigationRoutes.challenge.leaderboard);
          handleClose();
        }}
      >
        Challenge Board
      </MenuItem>
      <MenuItem
        onClick={() => {
          navigation(NavigationRoutes.challenge.parcipants);
          handleClose();
        }}
      >
        Participants
      </MenuItem>
    </Menu>
  );
}
