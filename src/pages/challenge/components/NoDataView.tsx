import { Stack } from "@mui/material";
import React from "react";
import resources from "../../../resources";

export default function NoDataView() {
  return (
    <Stack
      width="100%"
      padding={(theme) => theme.spacing(2)}
      alignItems="center"
      justifyContent="center"
    >
      <Stack
        width="400px"
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            width: "200px",
            height: "200px",
          },
        })}
        overflow="hidden"
        height="300px"
        alignSelf="center"
      >
        <img src={resources.nodata} alt="no-data-vector" />
      </Stack>
    </Stack>
  );
}
