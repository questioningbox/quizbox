import { IconButton, Stack, Typography } from "@mui/material";
import React, { useState } from "react";
import { BsTrophy } from "react-icons/bs";
import { MdOutlineKeyboardArrowDown } from "react-icons/md";
import ChallengeMenu from "./ChallengeMenu";

export default function PageHeader() {
  const [challengeMenu, setChallengeMenu] = useState<HTMLElement | null>(null);
  return (
    <Stack
      width="100%"
      direction="row"
      alignItems="center"
      justifyContent="space-between"
      padding={2}
    >
      <ChallengeMenu
        anchorEl={challengeMenu}
        handleClose={() => setChallengeMenu(null)}
      />
      <Typography variant="body1" fontWeight="bold">
        Challenge Request
      </Typography>
      <Stack direction="row" alignItems="center" justifyContent="flex-start">
        <BsTrophy />
        <IconButton
          onClick={(e) => setChallengeMenu(e.currentTarget)}
          size="small"
        >
          <MdOutlineKeyboardArrowDown />
        </IconButton>
      </Stack>
    </Stack>
  );
}
