import { faker } from "@faker-js/faker";
import dayjs from "dayjs";
import { generateId } from "../../../services";

export interface IFakeRequestIn {
  profileURL: string;
  username: string;
  id: string;
  datetime: string;
}

export function getIncomingChallenges(length: number = 0): IFakeRequestIn[] {
  const data: IFakeRequestIn[] = [];
  Array.from({ length }).map(() => {
    data.push({
      profileURL: faker.image.fashion(360, 360, true),
      id: generateId(),
      username: faker.name.fullName(),
      datetime: dayjs().format(),
    });
  });
  return data;
}
