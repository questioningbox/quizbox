import {
  CircularProgress,
  colors,
  Divider,
  Stack,
  Typography,
} from "@mui/material";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { AiOutlineCalendar } from "react-icons/ai";
import { IoIosArrowRoundBack } from "react-icons/io";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ContentLoader } from "../../../components";
import controller from "../../../controller";
import { setCompetitions } from "../../../features/slice/CompetitionReducer";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../../../features/slice/ResponseSlice";
import { PagedResults } from "../../../models";
import ApiResponseModel from "../../../models/ApiResponseModel";
import CompetitionModel, {
  CompetitionDetails,
} from "../../../models/CompetitionModel";
import ApiRoutes from "../../../routes/ApiRoutes";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { CustomIconButton, CustomPaginationView } from "../../../shared";
import { ChallengeCard } from "../components";

//
export default function ChallengeDetailsPage() {
  const [query] = useSearchParams();
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const competitionId = query.get("id");
  const [info, setInfo] = useState<CompetitionDetails | null>(null);
  const dispatch = useAppDispatch();
  const [pending, setPending] = useState<{ loading: boolean; userId: string }>({
    loading: false,
    userId: "",
  });
  const navigation = useNavigate();
  const { user } = useAppSelector((state) => state.UserReducer);
  const [relatedContest, setRelatedContest] = useState<
    PagedResults<CompetitionModel>
  >({ page: 0, pageSize: 10, totalDocuments: 0, totalPages: 0, results: [] });

  async function getCompetitionInfo() {
    try {
      dispatch(responsePending());
      const res = await controller<ApiResponseModel<CompetitionDetails>>({
        method: "get",
        url: `competition/${competitionId}`,
      });
      dispatch(responseSuccessful(res.message));
      setInfo(res.data);
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  async function getRelatedContest() {
    try {
      dispatch(responsePending());
      const res = await controller<
        ApiResponseModel<PagedResults<CompetitionModel>>
      >({
        method: "get",
        url: `competition/related/${competitionId}`,
        params: {
          page: 1,
          pageSize: 10,
        },
      });
      dispatch(responseSuccessful(res.message));
      setRelatedContest(res.data);
    } catch (error: any) {
      dispatch(responseFailed(error?.message || error));
    }
  }

  async function handleJoinCompetition() {
    try {
      dispatch(responsePending());
      const res = await controller<ApiResponseModel<CompetitionModel>>({
        method: "put",
        url: ApiRoutes.competition.crud(`join/${competitionId}`),
        token: user?.token,
      });
      dispatch(responseSuccessful(res.message));
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  async function handleRequest(status: string, userId: string) {
    try {
      setPending({ loading: true, userId });
      const res = await controller<ApiResponseModel<CompetitionDetails>>({
        method: "put",
        url: `competition/request/${userId}/${info?.competitionId}/${status}`,
        token: user?.token,
      });
      setInfo(res.data);
      dispatch(responseSuccessful(res.message));
      setPending({ loading: false, userId });
    } catch (error: any) {
      dispatch(responseFailed(error?.message || error));
    }
  }

  useEffect(() => {
    if (competitionId) {
      getRelatedContest();
      getCompetitionInfo();
    }
  }, []);
  return (
    <Stack alignItems="center" width="100%">
      {loading && <ContentLoader />}

      {info && !loading && (
        <>
          <Stack
            spacing={1.5}
            padding={2}
            bgcolor={(theme) => theme.palette.action.hover}
            width="100%"
          >
            <Stack width="100%" alignItems="center" justifyContent="center">
              <Typography textAlign="center" variant="h4">
                {info?.title}
              </Typography>
              <Typography
                style={{ fontStyle: "italic" }}
                variant="body1"
                fontWeight="bold"
                color="primary"
              >
                {info?.tag}
              </Typography>
            </Stack>
            <Divider />
            <Stack alignItems="center" justifyContent="center" width="100%">
              {info?.description}
            </Stack>
          </Stack>

          <Stack
            direction="row"
            spacing={1}
            bgcolor={(theme) =>
              dayjs(info?.startDate).diff(dayjs().format(), "seconds") <= 0
                ? theme.palette.error.main
                : theme.palette.primary.main
            }
            color={(theme) => theme.palette.common.white}
            padding={1}
            borderRadius={1}
            alignItems="center"
            justifyContent="center"
          >
            <AiOutlineCalendar />
            <Typography variant="body2">
              {dayjs(info?.startDate).format("dd, DD MMMM, YYYY")}
            </Typography>
            <Typography variant="body2">
              {dayjs(info?.startTime).format("hh:mm a")}
            </Typography>
          </Stack>
          <Divider />
          <Stack marginY={1} />
          <CustomIconButton
            size="small"
            variant="outlined"
            Icon={IoIosArrowRoundBack}
            title="Go Back"
            handleClick={() => navigation(NavigationRoutes.challenge.all)}
          />
          <Stack
            width="100%"
            alignItems="center"
            direction="row"
            spacing={1}
            justifyContent="center"
            padding={1}
          >
            {user &&
              !info.competitors.includes(user?.userId) &&
              info.request.filter((r) => r.info?.userId === user.userId)
                .length <= 0 &&
              info.createdBy.info?.userId !== user?.userId && (
                <CustomIconButton
                  handleClick={handleJoinCompetition}
                  size="small"
                  variant="outlined"
                  title="Join Challenge"
                />
              )}
          </Stack>
          {user &&
            user.userId === info.createdBy.info.userId &&
            info.request &&
            info.request.length > 0 && (
              <Stack
                width="100%"
                border={(theme) =>
                  `1px solid ${theme.palette.action.disabledBackground}`
                }
                padding={1.5}
                marginY={1.4}
                spacing={1}
                bgcolor={(theme) => theme.palette.action.hover}
              >
                <Stack>
                  <Typography variant="body1">Pending Request</Typography>
                </Stack>
                {info.request &&
                  info.request.length > 0 &&
                  info.request.map((r) => (
                    <Stack
                      border={(theme) =>
                        `1px solid ${theme.palette.action.disabledBackground}`
                      }
                      padding={1}
                      direction="row"
                      alignItems="center"
                      justifyContent="flex-start"
                      borderRadius={1}
                      spacing={1}
                      bgcolor={(theme) => theme.palette.common.white}
                    >
                      <Stack
                        width="30px"
                        height="30px"
                        overflow="hidden"
                        borderRadius="30px"
                        border={(theme) =>
                          `1px solid ${theme.palette.action.disabledBackground}`
                        }
                      >
                        {r.profile && r.profile.profileImage ? (
                          <img
                            className="img"
                            alt={r.info._id}
                            src={r.profile.profileImage.secureUrl}
                          />
                        ) : (
                          <Typography
                            fontWeight="bold"
                            color="primary"
                            variant="body1"
                          >
                            {r.info.name.charAt(0).toUpperCase()}
                          </Typography>
                        )}
                      </Stack>

                      <Typography variant="body1">{r.info.name}</Typography>
                      <Stack flex={1} />
                      <Stack direction="row" spacing={1}>
                        {pending.loading && pending.userId === r.info.userId ? (
                          <Stack width="30px" height="30px">
                            <CircularProgress size="small" />
                          </Stack>
                        ) : (
                          <>
                            <CustomIconButton
                              title="Approve"
                              variant="outlined"
                              size="xsmall"
                              handleClick={() =>
                                handleRequest("1", r.info.userId)
                              }
                            />
                            <CustomIconButton
                              title="Remove"
                              variant="outlined"
                              size="xsmall"
                              props={{
                                style: {
                                  borderColor: "firebrick",
                                  color: "firebrick",
                                },
                              }}
                              handleClick={() =>
                                handleRequest("0", r.info.userId)
                              }
                            />
                          </>
                        )}
                      </Stack>
                    </Stack>
                  ))}
              </Stack>
            )}
          {info.acceptedMembers && info.acceptedMembers.length > 0 && (
            <Stack spacing={1} width="100%" padding={1}>
              <Stack>
                <Typography variant="body1">Members</Typography>
              </Stack>
              {info &&
                info.acceptedMembers &&
                info.acceptedMembers.length > 0 &&
                info.acceptedMembers.map((r) => (
                  <Stack
                    border={(theme) =>
                      `1px solid ${theme.palette.action.disabledBackground}`
                    }
                    padding={1}
                    direction="row"
                    alignItems="center"
                    justifyContent="flex-start"
                    borderRadius={1}
                    spacing={1}
                    bgcolor={(theme) => theme.palette.common.white}
                  >
                    <Stack
                      width="30px"
                      height="30px"
                      overflow="hidden"
                      borderRadius="30px"
                      border={(theme) =>
                        `1px solid ${theme.palette.action.disabledBackground}`
                      }
                    >
                      {r.profile && r.profile.profileImage ? (
                        <img
                          className="img"
                          alt={r.info._id}
                          src={r.profile.profileImage.secureUrl}
                        />
                      ) : (
                        <Typography
                          fontWeight="bold"
                          color="primary"
                          variant="body1"
                        >
                          {r.info.name.charAt(0).toUpperCase()}
                        </Typography>
                      )}
                    </Stack>

                    <Typography variant="body1">{r.info.name}</Typography>
                    <Stack flex={1} />
                    <Stack direction="row" spacing={1}></Stack>
                  </Stack>
                ))}
            </Stack>
          )}
          {relatedContest.results.length > 0 ? (
            <Stack paddingY={2} spacing={1} height="100%" width="100%">
              {relatedContest.results.map((challenge) => (
                <ChallengeCard info={challenge} key={challenge._id} />
              ))}
              <Divider />
              <Stack alignItems="center" justifyContent="center">
                <CustomPaginationView
                  page={relatedContest.page}
                  pageSize={relatedContest.pageSize}
                  totalDocuments={relatedContest.totalDocuments}
                  totalPages={relatedContest.totalPages}
                />
              </Stack>
            </Stack>
          ) : (
            <Stack spacing={1.5} padding={5}>
              <Divider />
              <Typography
                textAlign="center"
                color={colors.grey[400]}
                variant="h5"
                sx={(theme) => ({
                  [theme.breakpoints.down("sm")]: {
                    fontSize: theme.spacing(3),
                  },
                })}
              >
                No Related Contest Available
              </Typography>
            </Stack>
          )}
        </>
      )}
    </Stack>
  );
}
