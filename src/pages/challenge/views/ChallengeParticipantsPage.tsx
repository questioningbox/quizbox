import { Divider, Stack, Typography } from "@mui/material";
import React from "react";
import { AiOutlineClockCircle } from "react-icons/ai";
import { RiGroupLine } from "react-icons/ri";
import resources from "../../../resources";
import { TfiBook } from "react-icons/tfi";
import { getFakeStudent, IFakeStudent } from "../../student/services";
import {
  ChallengeDetailCard,
  ChallengeHeaderTag,
  ContestantProfileCard,
  ContestResultsCard,
} from "../components";
import { AppColors } from "../../../constants";
import { SearchInput } from "../../../components";
import { HiUsers } from "react-icons/hi";

export interface IFakeProfile extends IFakeStudent {
  school: string;
}

export default function ChallengeParticipantsPage() {
  return (
    <Stack width="100%" height="100%">
      <Stack
        sx={(theme) => ({
          backgroundImage: `linear-gradient(rgba(0,0,0,0.45),rgba(0,0,0,0.25)),url(${resources.trophy})`,
          backgroundSize: "contain",
          height: "200px",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
        })}
        paddingX={(theme) => theme.spacing(4)}
        alignItems="baseline"
        justifyContent="space-between"
        width="100%"
      >
        <ChallengeHeaderTag />
        <Stack
          bgcolor={(theme) => theme.palette.common.white}
          marginBottom={(theme) => theme.spacing(-1)}
          padding={(theme) => theme.spacing(1.5)}
          alignSelf="center"
          width="100%"
          borderTop={(theme) => `3px solid ${theme.palette.primary.main}`}
        >
          <Stack
            spacing={2.5}
            direction="row"
            alignItems="center"
            justifyContent="space-between"
          >
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="flex-start"
              spacing={1}
            >
              <Typography
                sx={(theme) => ({
                  [theme.breakpoints.down("sm")]: {
                    display: "none",
                  },
                })}
                variant="body1"
              >
                Participants:
              </Typography>
              <Stack
                sx={(theme) => ({
                  [theme.breakpoints.up("md")]: {
                    display: "none",
                  },
                })}
              >
                <HiUsers size={24} color={AppColors.primary} />
              </Stack>
              <Typography variant="body1">
                10 <span style={{ color: "greeb" }}>users</span>
              </Typography>
            </Stack>
            <Stack direction="row" alignItems="center">
              <SearchInput />
            </Stack>
          </Stack>
        </Stack>
      </Stack>
      <Divider />
      <Stack padding={1.5} paddingX={4} width="100%"></Stack>
    </Stack>
  );
}
