import { CircularProgress, Divider, Stack, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../app/hooks";
import { TextLink } from "../../../components";

import {
  ActionHeader,
  ChallengeCard,
  NoDataView,
  PageHeader,
} from "../components";
import { getIncomingChallenges, IFakeRequestIn } from "../services";

export default function ReceivedChallengesPage() {
  const [loading, setLoading] = useState<boolean>(false);
  const { competitions } = useAppSelector((state) => state.CompetitionsReducer);
  function getChallenges() {}
  const [newChallenge, setNewChallenge] = useState(false);

  useEffect(() => {}, []);
  return (
    <Stack
      height="100%"
      width="100%"
      padding={(theme) => theme.spacing(2, 4)}
      sx={(theme) => ({
        overflowX: "hidden",
        overflowY: "auto",
      })}
      paddingBottom="100px"
    >
      <PageHeader />
      <ActionHeader handeAddNewChallenge={() => setNewChallenge(true)} />
      <Divider />
      {loading && (
        <Stack
          width="100%"
          alignItems="center"
          justifyContent="center"
          padding={2}
          height="100%"
          spacing={1.5}
        >
          <Stack>
            <CircularProgress />
          </Stack>
          <Typography textAlign="center" variant="h6">
            Loading....
          </Typography>
        </Stack>
      )}
      {!loading && competitions.results.length <= 0 && (
        <React.Fragment>
          <NoDataView />
          <Stack alignItems="center" justifyContent="center" width="100%">
            <Typography textAlign="center" variant="body2">
              looks like you don't have any challege request at the moment
            </Typography>
            <TextLink text="click here to add" />
          </Stack>
        </React.Fragment>
      )}
      {competitions.results.length > 0 && (
        <Stack spacing={1} height="100%" width="100%">
          {competitions.results.map((challenge) => (
            <ChallengeCard info={challenge} key={challenge._id} />
          ))}
        </Stack>
      )}
    </Stack>
  );
}
