import {
  Button,
  CircularProgress,
  Divider,
  Stack,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ContentLoader, TextLink } from "../../../components";
import {
  responseFailed,
  responseSuccessful,
} from "../../../features/slice/ResponseSlice";
import { CompetitionsThunk } from "../../../functions/slice";
import ApiRoutes from "../../../routes/ApiRoutes";
import { CustomPaginationView } from "../../../shared";
import { AddChallenge } from "../../../views";

import {
  ActionHeader,
  ChallengeCard,
  NoDataView,
  PageHeader,
} from "../components";

export default function UserChallengesPage() {
  const [newChallenge, setNewChallenge] = useState<boolean>(false);
  const { competitions, loading, error, message } = useAppSelector(
    (state) => state.CompetitionsReducer
  );
  const dispatch = useAppDispatch();

  //
  function getChallenges() {
    dispatch(
      CompetitionsThunk({ method: "get", url: ApiRoutes.competition.crud() })
    );
  }

  useEffect(() => {
    getChallenges();
    console.log(competitions);
  }, []);

  useEffect(() => {
    dispatch(responseFailed(error));
    dispatch(responseSuccessful(message));
  }, [error, message]);
  return (
    <Stack width="100%" padding={2}>
      <PageHeader />
      <ActionHeader handeAddNewChallenge={() => setNewChallenge(true)} />
      <AddChallenge
        open={newChallenge}
        handleClose={() => setNewChallenge(false)}
      />
      <Divider />
      {loading && <ContentLoader />}

      {!loading && competitions.results.length <= 0 && (
        <React.Fragment>
          <NoDataView />
          <Stack alignItems="center" justifyContent="center" width="100%">
            <Typography textAlign="center" variant="body2">
              looks like you don't have any challege request at the moment
            </Typography>
            <TextLink text="click here to add" />
          </Stack>
        </React.Fragment>
      )}
      {!loading && competitions.results.length > 0 && (
        <Stack paddingY={2} spacing={1} height="100%" width="100%">
          {competitions.results.map((challenge) => (
            <ChallengeCard info={challenge} key={challenge._id} />
          ))}
          <Divider />
          <Stack alignItems="center" justifyContent="center">
            <CustomPaginationView
              page={competitions.page}
              pageSize={competitions.pageSize}
              totalDocuments={competitions.totalDocuments}
              totalPages={competitions.totalPages}
            />
          </Stack>
        </Stack>
      )}
    </Stack>
  );
}
