import { Grid, Stack, Typography } from "@mui/material";
import { getgroups } from "process";
import React, { useEffect, useState } from "react";
import { AiOutlineUsergroupAdd } from "react-icons/ai";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ContentLoader, SearchInput } from "../../../components";
import { GroupsThunk } from "../../../functions/slice";
import ApiRoutes from "../../../routes/ApiRoutes";
import {
  CustomIconButton,
  CustomPaginationView,
  GroupCard,
} from "../../../shared";
import { CreateGroupModal, GroupInfoCard } from "../components";

export default function GroupsPage() {
  const [createGroup, setCreateGroup] = useState<boolean>(false);
  const { groups } = useAppSelector((state) => state.GroupsReducer);
  const dispatch = useAppDispatch();
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const { user } = useAppSelector((state) => state.UserReducer);
  function getGroups(
    page: number = groups.page,
    pageSize: number = groups.pageSize
  ) {
    dispatch(
      GroupsThunk({
        url: ApiRoutes.group.crud(),
        params: {
          page,
          pageSize,
          sortColumn: "createdAt",
          sortDir: "desc",
        },
        token: user?.token,
        method: "get",
      })
    );
  }

  useEffect(() => {
    getGroups(groups.page, groups.pageSize);
  }, []);
  return (
    <Stack width="100%" height="100%">
      <CreateGroupModal
        open={createGroup}
        handleClose={() => setCreateGroup(false)}
      />
      <Stack
        direction="row"
        padding={1.5}
        border={(theme) =>
          `1px solid ${theme.palette.action.disabledBackground}`
        }
        borderRadius={(theme) => theme.spacing(0.5)}
        alignItems="center"
        justifyContent="space-between"
      >
        <Typography variant="body1" color="primary">
          Groups
        </Typography>
        <Stack
          direction="row"
          spacing={1}
          alignItems="center"
          justifyContent="flex-end"
        >
          <SearchInput placeholder="search group..." />
          <CustomIconButton
            Icon={AiOutlineUsergroupAdd}
            title="Create"
            size="small"
            variant="contained"
            handleClick={() => setCreateGroup(true)}
          />
        </Stack>
      </Stack>
      <Stack spacing={1} width="100%" padding={2}>
        <Stack width="100%">
          {loading && <ContentLoader />}
          {!loading &&
            groups.results.map((g) => (
              <GroupCard info={g} key={g._id} />
              // <GroupInfoCard info={g} key={g._id} />
            ))}
        </Stack>
        {/* <CustomPaginationView
          page={groups.page}
          pageSize={groups.pageSize}
          totalDocuments={groups.totalDocuments}
          totalPages={groups.totalPages}
          title="Groups"
          handlePageChange={(page) => getGroups(page, groups.pageSize)}
          handlePageSize={(size) => getGroups(groups.page, size)}
        /> */}
      </Stack>
    </Stack>
  );
}
