import { Box, Button, Stack, Typography } from "@mui/material";
import React from "react";
import resources from "../../../resources";

export default function Banner() {
  return (
    <Stack
      width="100%"
      sx={(theme) => ({
        backgroundImage: `linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.55)),url(${resources.banner})`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
      })}
      alignItems="center"
      justifyContent="flex-end"
      position="relative"
      minHeight="20vh"
    >
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        alignSelf="bottom"
        padding={3}
        spacing={2}
        width="100%"
      >
        <Typography
          variant="h5"
          fontWeight={900}
          flex={1}
          color={(theme) => theme.palette.common.white}
        >
          Your number one plug for school items
        </Typography>
        <Button
          sx={(theme) => ({
            color: theme.palette.common.black,
            textTransform: "none",
            bgcolor: theme.palette.common.white,
            with: "100px",
          })}
          color="inherit"
          variant="contained"
          size="small"
        >
          Check Out Now
        </Button>
      </Stack>
    </Stack>
  );
}
