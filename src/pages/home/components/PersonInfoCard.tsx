import { colors, Stack, Typography } from "@mui/material";
import React from "react";
import { UserProfileInfo } from "../../../models/ProfileModel";
import { CustomIconButton } from "../../../shared";
import { useAppSelector } from "../../../app/hooks";

interface IProps {
  info: UserProfileInfo;
  isStudent?: boolean;
}
export default function PersonInfoCard({ info, isStudent }: IProps) {
  const { user } = useAppSelector((state) => state.UserReducer);
  return (
    <Stack
      padding={1}
      border={(theme) => `1px solid ${theme.palette.action.hover}`}
      borderRadius={0.5}
      bgcolor={(theme) => theme.palette.common.white}
      direction="row"
      alignItems="center"
      justifyContent="space-between"
      sx={(theme) => ({
        [theme.breakpoints.down("sm")]: {
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          justifyContent: "center",
          width: "100%",
        },
      })}
    >
      <Stack
        direction="row"
        spacing={1}
        alignItems="center"
        justifyContent="flex-start"
        width="100%"
      >
        <Stack
          width="35px"
          height="35px"
          borderRadius="35px"
          border={(theme) =>
            `1px solid ${theme.palette.action.disabledBackground}`
          }
          overflow="hidden"
          alignItems="center"
          justifyContent="center"
        >
          {info.profile && info.profile.profileImage ? (
            <img
              src={info.profile.profileImage.url}
              className="img"
              alt={`person-profile-${info.user._id}`}
            />
          ) : (
            <Typography fontWeight="bold" color="primary" variant="body1">
              {info.user.name.charAt(0)}
            </Typography>
          )}
        </Stack>
        <Stack>
          <Typography variant="body2">{info && info.user.name}</Typography>
          <Stack
            spacing={1}
            direction="row"
            alignItems="center"
            justifyContent="flex-start"
            marginY={-0.5}
          >
            <small>followers:</small>
            <small style={{ color: colors.blue.A700 }}>
              {info.profile ? info.profile.followers.length : 0}
            </small>
          </Stack>
        </Stack>
        <Stack flex={1} />
        {user &&
          info.profile &&
          user.userId !== info.user.userId &&
          !info.profile.followers.includes(user.userId) && (
            <Stack>
              <CustomIconButton
                size="xsmall"
                title="connect"
                variant="outlined"
              />
            </Stack>
          )}
      </Stack>
    </Stack>
  );
}
