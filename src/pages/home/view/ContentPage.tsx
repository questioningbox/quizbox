import { Stack } from "@mui/material";
import React from "react";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ContentLoader } from "../../../components";
import { AppFooter, HomeCategoryView } from "../../../views";
import { HomeBanner } from "../components";
import { StoreSection } from "../section";

export default function ContentPage() {
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  return (
    <Stack>
      <Stack
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            width: "100%",
          },
        })}
      >
        <HomeBanner />
        <HomeCategoryView />
        <Stack flex={1} width="100%" height="100%">
          <StoreSection />
        </Stack>
      </Stack>
    </Stack>
  );
}
