import { Button, Container, Stack, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { BsPatchCheck } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { AlertModal, PrimaryLinkButton } from "../../../components";
import controller from "../../../controller";
import {
  clearResponseMessages,
  setContest,
  setErrorMessage,
  setMessage,
} from "../../../features/slice/ContestReducer";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../../../features/slice/ResponseSlice";
import {
  ContestThunk,
  GetContestQuestionThunk,
} from "../../../functions/slice";
import FeaturesThunk from "../../../functions/slice/FeaturesThunk";
import ApiResponseModel from "../../../models/ApiResponseModel";
import ContestModel from "../../../models/ContestModel";
import ApiRoutes from "../../../routes/ApiRoutes";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { CustomIconButton } from "../../../shared";
import { ServiceUpgradeView } from "../../../views";
import { CreatePrepTestModal } from "../components";

export default function PreptestGuidePage() {
  const navigation = useNavigate();
  const dispatch = useAppDispatch();
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const { user } = useAppSelector((state) => state.UserReducer);
  const [create, setCreate] = useState(false);
  const [category, setCategory] = useState("");
  const { errorMessage, message, activeQuestion, contest } = useAppSelector(
    (state) => state.ContestReducer
  );
  async function initContest() {
    try {
      setCreate(false);
      dispatch(responsePending());
      const res = await controller<ApiResponseModel<ContestModel>>({
        method: "get",
        url: ApiRoutes.contest.crud(`new/${category}`),
        token: user?.token,
      });
      dispatch(setMessage(res.message));
      dispatch(responseSuccessful(null));
      dispatch(setContest(res.data));
    } catch (error) {
      dispatch(responseFailed(null));
      dispatch(setErrorMessage(error));
    }
  }

  async function handleStartContest() {
    dispatch(clearResponseMessages());
    dispatch(
      GetContestQuestionThunk({
        method: "get",
        url: ApiRoutes.contest.crud(`question/${contest?.contestId}`),
        token: user?.token,
      })
    );
  }

  function abandonTrialContest() {
    dispatch(
      ContestThunk({
        url: ApiRoutes.contest.crud(contest?.contestId),
        method: "delete",
        token: user?.token,
      })
    );
  }

  useEffect(() => {
    dispatch(FeaturesThunk({ method: "get", url: ApiRoutes.category.get }));
  }, []);

  return (
    <Stack height="100%" width="100%">
      <CreatePrepTestModal
        handleCategory={(cate) => setCategory(cate)}
        open={create}
        handleClose={() => setCreate(false)}
        handleInit={initContest}
      />
      <AlertModal
        isError={errorMessage ? errorMessage : undefined}
        isMessage={message ? message : undefined}
        text={errorMessage || message}
        action={
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="center"
            spacing={1}
          >
            {contest && (
              <CustomIconButton
                title="Abandon"
                variant="contained"
                handleClick={abandonTrialContest}
                props={{
                  style: {
                    borderColor: "firebrick",
                    backgroundColor: "firebrick",
                  },
                }}
              />
            )}
            {contest && !activeQuestion && (
              <CustomIconButton
                title="Continue"
                variant="contained"
                handleClick={handleStartContest}
                props={{
                  style: {
                    borderColor: "seagreen",
                    backgroundColor: "seagreen",
                  },
                }}
              />
            )}

            {contest && activeQuestion && (
              <CustomIconButton
                title="Start Test"
                variant="contained"
                handleClick={() => navigation(NavigationRoutes.preptest.exams)}
                props={{
                  style: {
                    borderColor: "seagreen",
                    backgroundColor: "seagreen",
                  },
                }}
              />
            )}

            {!contest && (
              <CustomIconButton
                title="Close"
                variant="outlined"
                size="small"
                handleClick={() => dispatch(clearResponseMessages())}
              />
            )}
          </Stack>
        }
      />
      <ServiceUpgradeView />
      <Stack
        component={Container}
        padding={4}
        alignItems="center"
        justifyContent="center"
      >
        <Stack marginBottom={2.5}>
          <BsPatchCheck size={28} style={{ color: "lightgreen" }} />
        </Stack>
        <Typography fontWeight={700} variant="body1">
          Ready to take test??
        </Typography>
        <Typography textAlign="justify" paddingY={2} variant="body2">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates
          illum eos inventore placeat. Deleniti animi ullam consequatur tenetur
          in officia voluptates accusantium debitis exercitationem aspernatur!
          Corporis in ipsam sapiente molestias? Consequatur iure suscipit
          necessitatibus excepturi eum tempora consectetur asperiores ullam
          velit modi laborum id nostrum voluptatibus vitae voluptate
          reprehenderit mollitia nesciunt, assumenda quisquam libero ipsa harum
          fugiat molestiae officia. Maiores? Maxime ab laboriosam repellendus
          error dignissimos illo
        </Typography>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          spacing={1}
          padding={1.5}
          width="100%"
        >
          <CustomIconButton
            size="small"
            handleClick={() => setCreate(true)}
            variant="contained"
            title="Take the test now"
          />
        </Stack>
      </Stack>
    </Stack>
  );
}
