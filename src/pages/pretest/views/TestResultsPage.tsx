import {
  Button,
  Container,
  Divider,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React from "react";
import { CiShare2 } from "react-icons/ci";
import { FiChevronRight } from "react-icons/fi";
import { IoTimeOutline } from "react-icons/io5";
import { RiHome6Line } from "react-icons/ri";
import {
  CustomIconButton,
  PrimaryLinkButton,
  TextLink,
} from "../../../components";
import { AppColors } from "../../../constants";
import resources from "../../../resources";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { CircularProgress, CircularProgressLabel } from "@chakra-ui/react";
import { StoreSection } from "../../home/section";
import { AppFooter } from "../../../views";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { CustomPaginationView } from "../../../shared";
import { calculateTotalScore } from "../services";
export default function TestResultsPage() {
  const isMobile = useMediaQuery("(max-width:567px)");
  const dispatch = useAppDispatch();
  const { user } = useAppSelector((state) => state.UserReducer);
  const { activeQuestion, contest } = useAppSelector(
    (state) => state.ContestReducer
  );
  return (
    <Stack height="100vh" paddingX={2}>
      <Stack
        paddingY={1.5}
        direction="row"
        alignItems="center"
        justifyContent="space-between"
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="flex-start"
          spacing={1}
        >
          <RiHome6Line size={15} style={{ color: "#c0c0c0" }} />
          <FiChevronRight style={{ color: "#c0c0c0" }} />
          <Typography variant="body2">Trial tests</Typography>
          <FiChevronRight style={{ color: "#c0c0c0" }} />
          <Typography variant="body2">...</Typography>
          <FiChevronRight style={{ color: "#c0c0c0" }} />
          <TextLink
            text={user?.name || ""}
            route={NavigationRoutes.profile.root}
          />
        </Stack>
      </Stack>
      <Stack
        width="100%"
        direction={isMobile ? "column" : "row"}
        alignItems="center"
        justifyContent="center"
      >
        <Stack alignItems="center" justifyContent="center" padding={1.5}>
          <Stack
            marginY={1.5}
            direction="row"
            alignItems="center"
            justifyContent="center"
            spacing={1}
          >
            {contest && (
              <Typography variant="h3">
                {(calculateTotalScore(contest?.answers).average * 100).toFixed(
                  0
                ) + "%"}
              </Typography>
            )}
            <sub>
              <Stack
                direction="row"
                alignItems="center"
                justifyContent="center"
                spacing={1}
              >
                <small>of</small>

                {contest && (
                  <Typography variant="body1">
                    {(
                      calculateTotalScore(contest?.answers).totalMark *
                      contest.answers.length
                    ).toFixed(0) + "%"}
                  </Typography>
                )}
              </Stack>
            </sub>
          </Stack>
          <Typography variant="body2">
            Weldone {user?.name}, doing great
          </Typography>
          <Stack
            direction="row"
            spacing={1}
            alignItems="center"
            justifyContent="center"
            padding={(theme) => theme.spacing(1, 0)}
            marginY={1}
          >
            <PrimaryLinkButton
              route={NavigationRoutes.preptest.guide}
              text="Take quize again"
            />
            <CustomIconButton
              props={{
                sx: (theme) => ({
                  border: `1px solid ${theme.palette.action.disabledBackground}`,
                  borderRadius: theme.spacing(0.5),
                  backgroundColor: theme.palette.common.white,
                  color: theme.palette.common.black,
                  "&:hover": {
                    bgcolor: theme.palette.action.hover,
                  },
                }),
              }}
              iconProps={{
                htmlColor: "#000",
                style: { color: "black", backgroundColor: "transparent" },
              }}
              Icon={CiShare2}
              title="Share results"
              handleClick={() => {}}
            />
          </Stack>
        </Stack>
      </Stack>
      <Stack marginY={2}>
        <Stack
          direction={isMobile ? "column" : "row"}
          alignItems="center"
          justifyContent="space-between"
          width="100%"
        >
          <Stack>
            <Typography
              textAlign={isMobile ? "center" : "inherit"}
              variant="body1"
              fontWeight={700}
            >
              Take a course to help you improve upon your performance
            </Typography>
            <Typography
              textAlign={isMobile ? "center" : "inherit"}
              variant="body2"
            >
              level up your understanding by taking a course
            </Typography>
          </Stack>
        </Stack>
      </Stack>
      <Divider />
      <Stack flex={1}>
        <StoreSection />
      </Stack>
      <Stack paddingY={2}>
        <CustomPaginationView
          totalDocuments={100}
          page={2}
          totalPages={11}
          pageSize={11}
        />
      </Stack>
    </Stack>
  );
}
