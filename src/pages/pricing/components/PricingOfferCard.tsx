import { Button, Divider, Grid, Stack, Typography } from "@mui/material";
import React from "react";
import { AiOutlineCheckCircle } from "react-icons/ai";
import { AppColors } from "../../../constants";
import PricingModel from "../../../models/PricingModel";

interface IProps {
  info: PricingModel;
}

export default function PricingOfferCard({ info }: IProps) {
  return (
    <Grid xs={10} md={4} sm={6} lg={3} xl={2} item>
      <Stack
        sx={(theme) => ({
          borderRadius: theme.spacing(0.85),
          border: `1px solid ${theme.palette.action.disabledBackground}`,
          "&:hover": {
            border: `1px solid ${theme.palette.action.hover}`,
            boxShadow: theme.shadows[1],
          },
          transition: "all 0.45s ease-in-out",
        })}
      >
        <Stack
          direction="row"
          width="100%"
          alignItems="center"
          justifyContent="space-between"
          padding={1}
        >
          <Typography variant="body2">{info.category.title} plan</Typography>
          <Typography
            sx={(theme) => ({
              padding: theme.spacing(0.5, 1.25),
              borderRadius: "30px",
              bgcolor: theme.palette.action.hover,
            })}
            variant="caption"
            color="primary"
          >
            {info.tag}
          </Typography>
        </Stack>
        <Stack padding={1}>
          <Typography variant="h4">
            ${info.rate.price}
            <sub style={{ color: "#c0c0c0", fontSize: "12px" }}>
              per {info.rate.title}
            </sub>
          </Typography>
        </Stack>
        <Stack padding={1}>
          <Typography variant="body2">{info.description}</Typography>
        </Stack>
        <Stack padding={1} marginY={1} spacing={1}>
          <Button
            sx={(theme) => ({ textTransform: "none" })}
            variant="contained"
            size="small"
            fullWidth
            color="primary"
          >
            Get started
          </Button>
          <Button
            sx={(theme) => ({ textTransform: "none" })}
            variant="outlined"
            size="small"
            fullWidth
          >
            Chat to sales
          </Button>
        </Stack>
        <Divider />
        <Stack padding={1}>
          <Typography variant="body1" fontWeight={600}>
            FEATURES
          </Typography>
          <Typography variant="caption">
            Everything in our free plan plus...
          </Typography>
        </Stack>
        <Stack spacing={0.5} padding={1} paddingBottom={2}>
          {info.features.map((feature) => {
            return (
              <Stack
                spacing={0.5}
                direction="row"
                width="100%"
                key={feature.id}
                alignItems="center"
                justifyContent="flex-start"
              >
                <AiOutlineCheckCircle style={{ color: AppColors.primary }} />
                <Typography
                  variant="body2"
                  fontSize={(theme) => theme.spacing(1.85)}
                >
                  {feature.feature}
                </Typography>
              </Stack>
            );
          })}
        </Stack>
      </Stack>
    </Grid>
  );
}
