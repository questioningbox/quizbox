import { Stack, Grid } from "@mui/material";
import React, { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import FeaturesThunk from "../../../functions/slice/FeaturesThunk";
import ApiRoutes from "../../../routes/ApiRoutes";
import { AppFooter } from "../../../views";
import { ContentHeader, PricingOfferCard } from "../components";
import { ContentLoader } from "../../../components";

export default function ContentPage() {
  const { user } = useAppSelector((state) => state.UserReducer);
  const { features } = useAppSelector((state) => state.FeaturesReducer);
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const dispatch = useAppDispatch();

  function getFeatures() {
    dispatch(
      FeaturesThunk({
        method: "get",
        url: ApiRoutes.category.get,
        token: user?.token,
      })
    );
  }

  useEffect(() => {
    getFeatures();
  }, []);

  return (
    <Stack width="100%" height="100%">
      <ContentHeader />
      <Stack flex={1} paddingBottom={3} alignItems="center" width="100%">
        <Grid
          spacing={1.5}
          container
          alignItems="center"
          justifyContent="center"
        >
          {loading && <ContentLoader />}
          {!loading &&
            features.pricing.map((p) => (
              <PricingOfferCard info={p} key={p._id} />
            ))}
        </Grid>
      </Stack>
    </Stack>
  );
}
