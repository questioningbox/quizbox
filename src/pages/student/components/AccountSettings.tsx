import { Menu, MenuItem } from "@mui/material";
import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { AppColors } from "../../../constants";
import NavigationRoutes from "../../../routes/NavigationRoutes";

interface IProps {
  anchorEl: HTMLElement | null;
  handleClose: () => void;
  handleDelete?: () => void;
}
export default function AccountSettings({
  anchorEl,
  handleClose,
  handleDelete,
}: IProps) {
  const navigation = useNavigate();
  const path = useLocation();
  return (
    <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
      <MenuItem
        sx={(theme) => ({
          height: "25px",
          color:
            path.pathname === NavigationRoutes.student.general
              ? AppColors.primary
              : "inherit",
        })}
        onClick={() => {
          handleClose();
          navigation(NavigationRoutes.student.general);
        }}
      >
        General
      </MenuItem>
      <MenuItem
        sx={(theme) => ({
          height: "25px",
          color:
            path.pathname === NavigationRoutes.student.edit
              ? AppColors.primary
              : "inherit",
        })}
        onClick={() => {
          handleClose();
          navigation(NavigationRoutes.student.edit);
        }}
      >
        Edit Profile
      </MenuItem>
      <MenuItem
        sx={(theme) => ({
          height: "25px",
          color:
            path.pathname === NavigationRoutes.student.notifications
              ? AppColors.primary
              : "inherit",
        })}
        onClick={() => {
          handleClose();
          navigation(NavigationRoutes.student.notifications);
        }}
      >
        Email Notifications
      </MenuItem>
    </Menu>
  );
}
