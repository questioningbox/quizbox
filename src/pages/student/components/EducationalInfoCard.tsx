import { IconButton, Stack, Typography } from "@mui/material";
import dayjs from "dayjs";
import React from "react";
import { CiBookmarkRemove } from "react-icons/ci";
import { FiEdit } from "react-icons/fi";
import { EducationalInfoModel } from "../../../models";

interface IProps {
  handleRemove: () => void;
  handleEdit: () => void;
  info: EducationalInfoModel;
  edit?: boolean;
}
export default function EducationalInfoCard({
  handleRemove,
  handleEdit,
  edit,
  info,
}: IProps) {
  return (
    <Stack
      sx={(theme) => ({
        boxShadow: edit ? theme.shadows[1] : theme.shadows[0],
      })}
      width="100%"
      padding={1}
    >
      <Typography variant="body1" fontWeight="bold">
        {info.schoolName}
      </Typography>
      <Typography variant="body1">{info.program}</Typography>
      <Stack
        direction="row"
        width="100%"
        alignItems="center"
        justifyContent="space-between"
      >
        <Typography variant="body2">
          {dayjs(info.startDate).format("YYYY")}-
          {info.endDate ? dayjs(info.endDate).format("YYYY") : "Present"}
        </Typography>
        <Stack
          spacing={1.5}
          direction="row"
          alignItems="center"
          justifyContent="flex-end"
        >
          {edit && (
            <React.Fragment>
              <IconButton onClick={handleRemove} size="small" color="error">
                <CiBookmarkRemove />
              </IconButton>
              <IconButton onClick={handleEdit} color="primary" size="small">
                <FiEdit />
              </IconButton>
            </React.Fragment>
          )}
        </Stack>
      </Stack>
    </Stack>
  );
}
