import { CircularProgress, IconButton, Stack, Typography } from "@mui/material";
import { profile } from "console";
import React, { useEffect, useState } from "react";
import { GrContactInfo } from "react-icons/gr";
import { SlUserFollow, SlUserUnfollow } from "react-icons/sl";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ContentLoader, ProfileAvatar } from "../../../components";
import controller from "../../../controller";
import { setProfiles } from "../../../features/auth/UserSlice";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../../../features/slice/ResponseSlice";
import { PagedResults } from "../../../models";
import ApiResponseModel from "../../../models/ApiResponseModel";
import ProfileModel, { UserProfileInfo } from "../../../models/ProfileModel";
import { UserInfo } from "../../../models/UserModel";
import ApiRoutes from "../../../routes/ApiRoutes";

//
interface IProps {
  info: UserProfileInfo;
  handleProfile: () => void;
}
export default function FriendCard({ info, handleProfile }: IProps) {
  const dispatch = useAppDispatch();
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const { user } = useAppSelector((state) => state.UserReducer);
  const [pending, setPending] = useState<boolean>(false);

  async function handleFollowUser() {
    try {
      setPending(true);
      const res = await controller<
        ApiResponseModel<PagedResults<UserProfileInfo>>
      >({
        method: "put",
        url: `profile/follow/${info.user.userId}`,
        token: user?.token,
      });
      setPending(false);
      dispatch(setProfiles(res.data));
    } catch (error: any) {
      setPending(false);
      dispatch(responseFailed(error?.message || error));
    }
  }
  return (
    <Stack
      border={(theme) => `1px solid ${theme.palette.action.hover}`}
      padding={1}
      direction="row"
      alignItems="center"
      justifyContent="flex-start"
      borderRadius={(theme) => theme.spacing(0.85)}
      marginY={1}
    >
      <ProfileAvatar
        size="small"
        name={!info.profile?.profileImage ? info?.user.name : ""}
        profileUrl={
          info.profile && info.profile.profileImage
            ? info?.profile?.profileImage.secureUrl
            : ""
        }
      />
      <Stack marginLeft={1.5} />
      <Stack>
        <Typography variant="body1" fontWeight="bold">
          {info.user?.name}
        </Typography>
        <Typography variant="caption"></Typography>
        <Stack spacing={1.5} direction="row">
          <Stack
            color={(theme) =>
              info.profile &&
              info.profile.followers.includes(user?.userId || "")
                ? theme.palette.primary.main
                : "inherit"
            }
            direction="row"
            spacing={0.5}
          >
            <Typography variant="caption">Following:</Typography>
            <Typography variant="caption">
              {info.profile ? info.profile?.following.length : 0}
            </Typography>
          </Stack>
          <Stack direction="row" spacing={0.5}>
            <Typography variant="caption">Followers:</Typography>
            <Typography variant="caption">
              {info.profile ? info.profile.followers.length : 0}
            </Typography>
          </Stack>
        </Stack>
      </Stack>
      <Stack flex={1} />
      <Stack spacing={1} direction="row">
        {pending && (
          <Stack width="30px" height="30px">
            <CircularProgress size="small" />
          </Stack>
        )}
        {!pending && (
          <>
            <IconButton onClick={handleProfile} color="primary" size="small">
              <GrContactInfo color="inherit" />
            </IconButton>
            {info.profile &&
            info.profile.followers.includes(user?.userId || "") ? (
              <IconButton
                onClick={handleFollowUser}
                size="small"
                color="default"
              >
                <SlUserUnfollow />
              </IconButton>
            ) : (
              info.profile && (
                <IconButton
                  onClick={handleFollowUser}
                  size="small"
                  color="primary"
                >
                  <SlUserFollow />
                </IconButton>
              )
            )}
          </>
        )}
      </Stack>
    </Stack>
  );
}
