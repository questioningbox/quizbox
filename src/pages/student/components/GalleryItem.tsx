import { Grid, Stack } from "@mui/material";
import React from "react";

interface IProps {
  image: any;
}
export default function GalleryItem({ image }: IProps) {
  return (
    <Grid width="150px" item>
      <Stack height="200px" width="100%" overflow="hidden">
        <img src={image} alt={`profile-gallery-${image}`} />
      </Stack>
    </Grid>
  );
}
