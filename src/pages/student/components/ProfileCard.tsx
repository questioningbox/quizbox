import { Stack, Typography } from "@mui/material";
import React from "react";
import { useAppSelector } from "../../../app/hooks";
import { PrimaryLinkButton, TextLink } from "../../../components";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { IFakeStudent } from "../services";

export default function ProfileCard() {
  const { user, profile } = useAppSelector((state) => state.UserReducer);
  return (
    <Stack alignItems="center" justifyContent="center" padding={4}>
      <Stack
        overflow="hidden"
        border={(theme) => `1px solid ${theme.palette.action.hover}`}
        width="80px"
        height="80px"
        borderRadius="80px"
        justifyContent="center"
        alignItems="center"
        bgcolor={(theme) => theme.palette.action.hover}
      >
        {profile && profile.profileImage ? (
          <img
            className="img"
            src={profile.profileImage.secureUrl}
            alt={`profile-picture-${profile.profileImage.assetId}`}
          />
        ) : (
          <Typography variant="h4" color="primary">
            {user && user.name.charAt(0).toUpperCase()}
          </Typography>
        )}
      </Stack>
      <Stack marginTop={0.5} />
      <Typography variant="body1" fontWeight="bold">
        {user?.name}
      </Typography>

      <TextLink
        props={{
          sx: (theme) => ({
            color: theme.palette.common.black,
            textDecoration: "none",
            fontSize: theme.spacing(1.5),
          }),
        }}
        text={user ? user.username : ""}
      />
      <Stack
        spacing={2}
        direction="row"
        alignItems="center"
        justifyContent="center"
        marginTop={2}
      >
        <PrimaryLinkButton
          text="View Profile"
          props={{
            sx: (theme) => ({
              borderRadius: theme.spacing(0.5),
              bgcolor: theme.palette.primary.main,
              color: theme.palette.common.white,
              textDecoration: "none",
              transition: "all 0.45s ease-in-out",
              fontSize: theme.spacing(1.5),
              padding: theme.spacing(0.5, 1),
              "&:hover": {
                bgcolor: theme.palette.primary.dark,
              },
            }),
          }}
          route={NavigationRoutes.student.general}
        />
        <PrimaryLinkButton
          text="Edit Profile"
          props={{
            sx: (theme) => ({
              borderRadius: theme.spacing(0.5),
              bgcolor: theme.palette.primary.main,
              color: theme.palette.common.white,
              textDecoration: "none",
              transition: "all 0.45s ease-in-out",
              fontSize: theme.spacing(1.5),
              padding: theme.spacing(0.5, 1),
              "&:hover": {
                bgcolor: theme.palette.primary.dark,
              },
            }),
          }}
          route={NavigationRoutes.student.edit}
        />
      </Stack>
    </Stack>
  );
}
