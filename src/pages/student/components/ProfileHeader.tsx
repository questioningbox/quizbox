import { IconButton, Stack, Typography } from "@mui/material";
import React, { MouseEvent } from "react";
import { MdKeyboardArrowDown } from "react-icons/md";
import { useAppSelector } from "../../../app/hooks";
import { ProfileAvatar } from "../../../components";

interface IProps {
  handleProfileMenu: (e: MouseEvent<HTMLButtonElement>) => void;
  pageTitle?: string;
}
export default function ProfileHeader({
  handleProfileMenu,
  pageTitle,
}: IProps) {
  const { user } = useAppSelector((state) => state.UserReducer);
  return (
    <Stack
      width="100%"
      direction="row"
      alignItems="center"
      justifyContent="space-between"
      padding={(theme) => theme.spacing(2)}
    >
      <Stack alignItems="center" direction="row" justifyContent="flex-start">
        {pageTitle && (
          <Typography fontWeight="bold" variant="body1">
            {pageTitle}
          </Typography>
        )}
      </Stack>
      <Stack alignItems="center" justifyContent="flex-end" direction="row">
        <ProfileAvatar />
        <Stack marginLeft={1} />
        {/* <Typography variant="body2">{user?.name}</Typography> */}
        <IconButton size="small" onClick={handleProfileMenu}>
          <MdKeyboardArrowDown />
        </IconButton>
      </Stack>
    </Stack>
  );
}
