import { Menu, MenuItem } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import NavigationRoutes from "../../../routes/NavigationRoutes";

interface IProps {
  handleClose: () => void;
  anchorEl: HTMLElement | null;
}
export default function ProfileMenu({ handleClose, anchorEl }: IProps) {
  const navigation = useNavigate();
  return (
    <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
      <MenuItem
        onClick={() => {
          handleClose();
          navigation(NavigationRoutes.student.general);
        }}
      >
        View Profile
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleClose();
          navigation(NavigationRoutes.student.edit);
        }}
      >
        Edit Profile
      </MenuItem>
      <MenuItem onClick={handleClose}>Performance and Analytics</MenuItem>
      <MenuItem
        sx={(theme) => ({
          color: theme.palette.error.main,
        })}
        onClick={handleClose}
      >
        Logout
      </MenuItem>
    </Menu>
  );
}
