import { IconButton, Stack, Typography } from "@mui/material";
import dayjs from "dayjs";
import React from "react";
import { CiBookmarkRemove } from "react-icons/ci";
import { FiEdit } from "react-icons/fi";
import { EducationalInfoModel, WorkExperienceInfoModel } from "../../../models";

interface IProps {
  handleRemove: () => void;
  handleEdit: () => void;
  info: WorkExperienceInfoModel;
  edit?: boolean;
}
export default function WorkExperienceInfoCard({
  handleRemove,
  handleEdit,
  edit,
  info,
}: IProps) {
  return (
    <Stack
      sx={(theme) => ({
        boxShadow: edit ? theme.shadows[1] : theme.shadows[0],
      })}
      width="100%"
      padding={1}
    >
      <Typography fontWeight="bold" variant="body1">
        Company: {info.company}
      </Typography>
      <Typography variant="body1">JobTitle: {info.jobTitle}</Typography>
      <Typography variant="body1">Role/Position: {info.role}</Typography>
      <Stack
        direction="row"
        width="100%"
        alignItems="center"
        justifyContent="space-between"
      >
        <Typography variant="body2">
          {dayjs(info.startDate).format("YYYY")}-
          {info.endDate ? dayjs(info.endDate).format("YYYY") : "Present"}
        </Typography>
        {edit && (
          <Stack
            spacing={1.5}
            direction="row"
            alignItems="center"
            justifyContent="flex-end"
          >
            <IconButton onClick={handleRemove} size="small" color="error">
              <CiBookmarkRemove />
            </IconButton>
            <IconButton onClick={handleEdit} color="primary" size="small">
              <FiEdit />
            </IconButton>
          </Stack>
        )}
      </Stack>
    </Stack>
  );
}
