import { Stack } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ContentLoader, SearchInput } from "../../../components";
import controller from "../../../controller";
import { setProfiles } from "../../../features/auth/UserSlice";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../../../features/slice/ResponseSlice";
import { PagedResults } from "../../../models";
import ApiResponseModel from "../../../models/ApiResponseModel";
import ProfileModel, { UserProfileInfo } from "../../../models/ProfileModel";
import ApiRoutes from "../../../routes/ApiRoutes";
import { FriendCard } from "../components";

interface IProps {
  handleProfile: (profile: ProfileModel) => void;
}
export default function StudentFriendsTab({ handleProfile }: IProps) {
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const { user, profiles } = useAppSelector((state) => state.UserReducer);
  const dispatch = useAppDispatch();

  async function handleProfiles() {
    try {
      dispatch(responsePending());
      const data = await controller<
        ApiResponseModel<PagedResults<UserProfileInfo>>
      >({
        method: "get",
        url: "profile/friends",
        token: user?.token || "",
        data: null,
      });
      dispatch(responseSuccessful(data.message));
      dispatch(setProfiles(data.data));
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  useEffect(() => {
    handleProfiles();
  }, []);
  return (
    <Stack width="100%">
      <SearchInput placeholder="search friends..." />

      <Stack width="100%">
        {loading && <ContentLoader />}
        {!loading &&
          profiles &&
          profiles.results
            .filter((f) => f.profile !== null)
            .map((profile) => (
              <FriendCard
                handleProfile={() => handleProfile(profile.profile)}
                info={profile}
              />
            ))}
      </Stack>
    </Stack>
  );
}
