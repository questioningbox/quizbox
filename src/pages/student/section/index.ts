export { default as AnalyticTab } from "./AnalyticTab";
export { default as UserFriendsTab } from "./UserFriendsTab";
export { default as CoursesTab } from "./CoursesTab";
