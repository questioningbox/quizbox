import { v4 } from "uuid";
import { faker } from "@faker-js/faker";

export interface IFakeStudent {
  name: string;
  profileURL: string;
  id: string;
  email: string;
}

export function getFakeStudent(): IFakeStudent {
  return {
    name: faker.name.fullName(),
    id: v4(),
    email: `${faker.name.firstName().toLowerCase()}@gmail.com`,
    profileURL: faker.image.fashion(360, 360, true),
  };
}

export function getGalleryImages(length: number = 5): string[] {
  const data: string[] = [];
  Array.from({ length }).map(() => {
    data.push(faker.image.fashion(500, 500, true));
  });
  return data;
}
