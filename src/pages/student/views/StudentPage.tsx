import { Stack } from "@mui/material";
import React, { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { UserProfileThunk } from "../../../functions/slice";
import ApiRoutes from "../../../routes/ApiRoutes";
import NavigationRoutes from "../../../routes/NavigationRoutes";

export default function StudentPage() {
  const { user } = useAppSelector((state) => state.UserReducer);
  const navigation = useNavigate();
  const dispatch = useAppDispatch();
  useEffect(() => {
    !user && navigation(NavigationRoutes.home.root);
  }, [user]);

  useEffect(() => {
    if (user) {
      dispatch(
        UserProfileThunk({
          method: "get",
          url: ApiRoutes.profile.crud,
          token: user.token,
          data: null,
        })
      );
    }
  }, []);
  return (
    <Stack
      height="100%"
      width="100vw"
      alignItems="center"
      justifyContent="flex-start"
    >
      <Outlet />
    </Stack>
  );
}
