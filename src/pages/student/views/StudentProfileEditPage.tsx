import {
  Button,
  Chip,
  Divider,
  IconButton,
  MenuItem,
  Stack,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { ChangeEvent, useEffect, useState } from "react";
import {
  AccountSettings,
  EducationalInfoCard,
  ProfileHeader,
  ProfileMenu,
  ProfileSettingsMenu,
  WorkExperienceInfoCard,
} from "../components";
import { HiOutlineHome } from "react-icons/hi";
import {
  CustomIconButton,
  CustomInkButton,
  InputGroup,
  ProfileInfoCard,
  TextLink,
} from "../../../components";
import { CiEdit, CiPhone, CiUser } from "react-icons/ci";
import { HiOutlineEnvelope } from "react-icons/hi2";
import { AiOutlineClose, AiOutlineCloudUpload } from "react-icons/ai";
import { MdOutlineMoreVert } from "react-icons/md";
import NavigationRoutes from "../../../routes/NavigationRoutes";
import { useLocation, useNavigate } from "react-router-dom";
import {
  CustomModal,
  ProfileViewModal,
  WorkExperienceInfoModal,
} from "../../../shared";
import { AppColors } from "../../../constants";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { FiCamera, FiChevronDown } from "react-icons/fi";
import { IoMdAddCircleOutline } from "react-icons/io";
import { UserProfileThunk } from "../../../functions/slice";
import ApiRoutes from "../../../routes/ApiRoutes";
import ProfileModel, { ProfileDto } from "../../../models/ProfileModel";
import EducationalInfoModal from "../../../shared/EducationalInfoModal";
import { EducationalInfoModel, WorkExperienceInfoModel } from "../../../models";
import { BsInfo } from "react-icons/bs";

//
export default function ProfileEditPage() {
  const [addEducation, setAddEducation] = useState<boolean>(false);
  const [addWork, setAddWork] = useState<boolean>(false);
  const [work, setWork] = useState<WorkExperienceInfoModel | null>(null);
  const [school, setSchool] = useState<EducationalInfoModel | null>(null);
  const [profileInfo, setProfileInfo] = useState<ProfileDto>({
    address: "",
    workExperience: [],
    educationalInfo: [],
    socialLinks: [],
  });
  const { user, profile } = useAppSelector((state) => state.UserReducer);
  const navigation = useNavigate();
  const path = useLocation();
  const dispatch = useAppDispatch();
  const [profilePicturePreview, setProfilePicturePreview] = useState<any>(null);
  const [deleteAccount, setDeleteAccount] = useState<boolean>(false);
  const [accountSettings, setAccountSettings] = useState<HTMLElement | null>(
    null
  );
  const [profileImage, setProfileImage] = useState<File | null>(null);
  const [profileSettingsMenu, setProfileSettingsMenu] =
    useState<HTMLElement | null>(null);
  const isMobile = useMediaQuery("(max-width:567px)");

  function handleProfilePictureUpload() {
    const formData = new FormData();
    if (profileImage) {
      formData.append("file", profileImage);
      dispatch(
        UserProfileThunk({
          method: "post",
          token: user?.token,
          url: ApiRoutes.profile.updatePicture,
          data: formData,
          isFile: true,
        })
      );
    }
  }

  function handleProfile() {
    console.log(profileInfo);
    if (profile) {
      dispatch(
        UserProfileThunk({
          method: "put",
          data: profileInfo as any,
          token: user ? user.token : "",
          url: ApiRoutes.profile.crud,
        })
      );
    } else {
      dispatch(
        UserProfileThunk({
          method: "post",
          data: profileInfo,
          token: user ? user.token : "",
          url: ApiRoutes.profile.crud,
        })
      );
    }
  }

  useEffect(() => {
    if (profileImage) {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(profileImage);
      fileReader.addEventListener("load", (e) => {
        setProfilePicturePreview(e.target?.result);
      });
    }
  }, [profileImage]);

  useEffect(() => {
    if (profile) {
      setProfileInfo({
        ...profileInfo,
        address: profile.address,
        workExperience: profile.workExperience,
        educationalInfo: profile.educationalInfo,
        socialLinks: profile.socialLinks,
      });
    }
  }, []);

  return (
    <Stack
      padding={(theme) => theme.spacing(2, 4)}
      width="100%"
      height="100%"
      sx={(theme) => ({
        overflowX: "hidden",
        overflowY: "auto",
        paddingBottom: "150px",
        [theme.breakpoints.down("sm")]: {
          padding: theme.spacing(2, 0.5),
          paddingBottom: "150px",
        },
      })}
    >
      <EducationalInfoModal
        school={school}
        handleAddSchool={(val) => {
          {
            if (school) {
              setProfileInfo({
                ...profileInfo,
                educationalInfo: profileInfo.educationalInfo.map((ed) => {
                  if (ed.id === val.id) {
                    return val;
                  } else {
                    return ed;
                  }
                }),
              });
            } else {
              setProfileInfo({
                ...profileInfo,
                educationalInfo: [...profileInfo.educationalInfo, val],
              });
            }
          }
        }}
        open={addEducation || Boolean(school)}
        handleClose={() => {
          setAddEducation(false);
          setSchool(null);
        }}
      />

      <WorkExperienceInfoModal
        work={work}
        handleClose={() => {
          setAddWork(false);
          setWork(null);
        }}
        handleAddWork={(val) => {
          if (work) {
            setProfileInfo({
              ...profileInfo,
              workExperience: profileInfo.workExperience.map((wrk) => {
                if (wrk.id === val.id) {
                  return val;
                } else {
                  return wrk;
                }
              }),
            });
          } else {
            setProfileInfo({
              ...profileInfo,
              workExperience: [...profileInfo.workExperience, val],
            });
          }
        }}
        open={addWork || Boolean(work)}
      />
      <ProfileMenu
        anchorEl={profileSettingsMenu}
        handleClose={() => setProfileSettingsMenu(null)}
      />
      <AccountSettings
        handleDelete={() => setDeleteAccount(true)}
        anchorEl={accountSettings}
        handleClose={() => setAccountSettings(null)}
      />
      {user ? (
        <ProfileHeader
          handleProfileMenu={(e) => setProfileSettingsMenu(e.currentTarget)}
        />
      ) : null}

      <CustomModal
        props={{
          open: deleteAccount,
          maxWidth: "sm",
          sx: (theme) => ({
            height: "250px",
          }),
        }}
        Header={
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="space-between"
            width="100%"
            overflow="hidden"
          >
            <Typography fontWeight="bold" variant="body1">
              Delete Account
            </Typography>

            <IconButton onClick={() => setDeleteAccount(false)} size="small">
              <AiOutlineClose />
            </IconButton>
          </Stack>
        }
        DialogAction={
          <Stack
            width="100%"
            alignItems="center"
            justifyContent="flex-end"
            spacing={1.5}
            direction="row"
            paddingX={2}
          >
            <Button
              sx={(theme) => ({
                textTransform: "none",
              })}
              variant="outlined"
              size="small"
              color="inherit"
            >
              No,keep it
            </Button>
            <Button
              sx={(theme) => ({
                textTransform: "none",
              })}
              variant="contained"
              size="small"
              color="error"
            >
              Yes Delete
            </Button>
          </Stack>
        }
        open={deleteAccount}
      >
        <Stack spacing={1}>
          <Typography variant="body2">
            Are you sure you want to delete your account? this action cannot be
            undone
          </Typography>
        </Stack>
      </CustomModal>

      <Stack alignItems="center" justifyContent="flex-start" spacing={1}>
        <Stack
          width={isMobile ? "100%" : "600px"}
          padding={2}
          alignItems="flex-start"
          justifyContent="center"
        >
          <Stack
            alignItems="center"
            justifyContent="center"
            spacing={2}
            direction="row"
          >
            <Stack
              width="60px"
              height="60px"
              borderRadius="60px"
              overflow="hidden"
              justifyContent="center"
              alignItems="center"
              border={(theme) => `1px solid ${theme.palette.action.hover}`}
              bgcolor={(theme) => theme.palette.action.hover}
            >
              <Typography variant="h4" color="primary">
                {user && user.name.charAt(0).toUpperCase()}
              </Typography>
            </Stack>
            <Stack flex={isMobile ? 1 : undefined}>
              <Stack
                alignItems="center"
                justifyContent={"flex-start"}
                direction="row"
                spacing={1}
              >
                <HiOutlineHome />
                <Typography variant="caption">General</Typography>
              </Stack>
              <Typography variant="body1" fontWeight="bold">
                {user?.name}
              </Typography>
              {user ? (
                <TextLink
                  props={{
                    sx: (theme) => ({
                      textDecoration: "none",
                      color: theme.palette.common.black,
                      fontSize: theme.spacing(1.5),
                    }),
                  }}
                  text={user ? user.username : ""}
                />
              ) : null}
            </Stack>
            {isMobile ? (
              <IconButton
                onClick={(e) => setAccountSettings(e.currentTarget)}
                size="small"
              >
                <MdOutlineMoreVert />
              </IconButton>
            ) : null}
          </Stack>
        </Stack>
        <Stack
          direction={isMobile ? "column" : "row"}
          padding={2}
          paddingTop={1}
          alignItems="center"
          justifyContent="center"
          width={isMobile ? "100%" : "600px"}
          spacing={1}
        >
          {isMobile ? null : (
            <Stack
              paddingRight={1}
              borderRight={(theme) =>
                `${isMobile ? "0" : "1"} solid ${theme.palette.action.hover}`
              }
              spacing={0.5}
            >
              <MenuItem
                sx={(theme) => ({
                  height: "30px",
                  fontSize: theme.spacing(1.45),
                  color:
                    path.pathname === NavigationRoutes.student.general
                      ? AppColors.primary
                      : "inherit",
                })}
                onClick={() => {
                  navigation(NavigationRoutes.student.general);
                }}
              >
                General
              </MenuItem>

              <MenuItem
                sx={(theme) => ({
                  height: "30px",
                  fontSize: theme.spacing(1.45),
                  color:
                    path.pathname === NavigationRoutes.student.notifications
                      ? AppColors.primary
                      : "inherit",
                })}
                onClick={() => {
                  navigation(NavigationRoutes.student.notifications);
                }}
              >
                Email Notification
              </MenuItem>
              <MenuItem
                sx={(theme) => ({
                  height: "30px",
                  color: theme.palette.error.main,
                  fontSize: theme.spacing(1.45),
                })}
                onClick={() => setDeleteAccount(true)}
              >
                Delete Profile
              </MenuItem>
            </Stack>
          )}
          {user ? (
            <Stack
              padding={(theme) => theme.spacing(0, 2)}
              width={isMobile ? "100%" : "400px"}
              spacing={1}
            >
              <ProfileInfoCard
                label="FullName"
                value={user.name}
                subtitle="this name will be showed on your profile"
                Icon={<CiUser />}
              />
              {user.email && (
                <ProfileInfoCard
                  value={user.email}
                  label="Email"
                  Icon={<HiOutlineEnvelope />}
                />
              )}
              {user.phoneNumber && (
                <ProfileInfoCard
                  value={user.phoneNumber}
                  label="PhoneNumber"
                  Icon={<CiPhone />}
                />
              )}
              <Stack>
                <TextLink text="Upgrade to premium" />
                <Typography variant="caption">
                  upgrade to get access to all features now
                </Typography>
              </Stack>
              {false && (
                <Button
                  size="small"
                  color="primary"
                  variant="contained"
                  sx={(theme) => ({
                    alignSelf: "flex-end",
                    textTransform: "none",
                  })}
                >
                  Save Changes
                </Button>
              )}
            </Stack>
          ) : null}
        </Stack>
      </Stack>
      <Divider />

      <Stack marginTop={3} />
      <Stack alignItems="center">
        <Stack position="relative">
          <Stack
            width="200px"
            height="200px"
            borderRadius="200px"
            border={(theme) => `1px solid ${theme.palette.action.hover}`}
            sx={(theme) => ({
              [theme.breakpoints.down("sm")]: {
                width: "150px",
                height: "150px",
                borderRadius: "150px",
              },
            })}
            bgcolor={(theme) => theme.palette.action.hover}
            justifyContent="center"
            alignItems="center"
            overflow="hidden"
            position="relative"
          >
            <img
              src={
                profilePicturePreview
                  ? profilePicturePreview
                  : profile
                  ? profile.profileImage
                    ? profile.profileImage.secureUrl
                    : ""
                  : ""
              }
              alt="ppp"
              className="img"
            />
          </Stack>
          <Stack
            sx={(theme) => ({
              position: "absolute",
              bottom: 0,
              zIndex: 1001,
              right: 5,
            })}
          >
            <Typography
              sx={(theme) => ({
                width: "40px",
                height: "40px",
                borderRadius: "40px",
                justifyContent: "center",
                alignItems: "center",
                border: `1px solid ${theme.palette.action.hover}`,
                bgcolor: theme.palette.action.hover,
                display: "flex",
                cursor: "pointer",
                "&:hover": {
                  bgcolor: theme.palette.action.disabledBackground,
                },
                transition: "all 0.45s ease-in-out",
              })}
              component="label"
              htmlFor="file-input"
              color="primary"
            >
              <FiCamera />
            </Typography>
            <TextField
              onChange={(e: ChangeEvent<HTMLInputElement>) => {
                e.target.files && setProfileImage(e.target.files[0]);
              }}
              sx={(theme) => ({
                display: "none",
              })}
              id="file-input"
              type="file"
            ></TextField>
          </Stack>
        </Stack>
        {profileImage && (
          <Stack marginTop={4}>
            <CustomIconButton
              handleClick={handleProfilePictureUpload}
              title="Upload"
              Icon={AiOutlineCloudUpload}
            />
          </Stack>
        )}
        <Stack width="100%"></Stack>
        <Divider />
        <Stack width="100%" spacing={1} padding={2}>
          <InputGroup
            handleChange={(e) =>
              setProfileInfo({ ...profileInfo, address: e.target.value })
            }
            placeholder="enter your address"
            label="Address"
            props={{
              value: profileInfo?.address,
            }}
          />
          <Stack
            direction="row"
            spacing={1}
            alignItems="center"
            justifyContent="center"
            width="100%"
            padding={0.5}
          >
            <Typography style={{ flex: 1 }} textAlign="left" variant="body1">
              Educational Background
            </Typography>
            <CustomInkButton handleClick={() => setAddEducation(true)}>
              <IoMdAddCircleOutline color={AppColors.primary} />
            </CustomInkButton>
          </Stack>
          <Divider />
          {profileInfo.educationalInfo.map((edu) => (
            <EducationalInfoCard
              edit
              handleEdit={() => setSchool(edu)}
              key={edu.id}
              info={edu}
              handleRemove={() =>
                setProfileInfo({
                  ...profileInfo,
                  educationalInfo: profileInfo.educationalInfo.filter(
                    (e) => e.id !== edu.id
                  ),
                })
              }
            />
          ))}
          <Stack
            direction="row"
            spacing={1}
            alignItems="center"
            justifyContent="center"
            width="100%"
            padding={0.5}
          >
            <Typography style={{ flex: 1 }} textAlign="left" variant="body1">
              Work Experience
            </Typography>
            <CustomInkButton handleClick={() => setAddWork(true)}>
              <IoMdAddCircleOutline color={AppColors.primary} />
            </CustomInkButton>
          </Stack>
          <Divider />

          {profileInfo.workExperience.map((wrk) => (
            <WorkExperienceInfoCard
              edit
              handleRemove={() =>
                setProfileInfo({
                  ...profileInfo,
                  workExperience: profileInfo.workExperience.filter(
                    (w) => w.id !== wrk.id
                  ),
                })
              }
              info={wrk}
              handleEdit={() => setWork(wrk)}
            />
          ))}
          <Divider />
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="flex-end"
            width="100%"
            padding={1.5}
          >
            <CustomIconButton
              title={profile ? "Save Changes" : "Submit"}
              Icon={BsInfo}
              handleClick={handleProfile}
            />
          </Stack>
        </Stack>
      </Stack>
    </Stack>
  );
}
