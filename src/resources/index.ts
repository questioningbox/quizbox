import avatar1 from "../assets/avatar1.jpg";
import avatar2 from "../assets/avatar2.jpg";
import avatar3 from "../assets/avatar3.jpg";
import avatar4 from "../assets/avatar5.jpg";
import avatar5 from "../assets/avatar5.jpg";
import banner from "../assets/banner1.jpg";
import cate from "../assets/cate.jpg";
import book from "../assets/book.jpg";
import banner2 from "../assets/banner3.jpg";
import nodata from "../assets/no-data.png";
import trophy from "../assets/trophy.png";
import groupinfobanner from "../assets/group-info-banner.jpg";
///
export const avatarImages = {
  avatar1,
  avatar2,
  avatar3,

  avatar4,
  avatar5,
};

export default { banner, cate, book, banner2, nodata, trophy, groupinfobanner };
