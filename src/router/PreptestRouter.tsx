import React from "react";
import { Route, Routes } from "react-router-dom";
import { AuthGuard } from "../pages/auth/view";
import {
  ContentPage,
  PreptestGuidePage,
  RootPage,
  TestPage,
  TestResutlsPage,
} from "../pages/pretest/views";
import NavigationRoutes from "../routes/NavigationRoutes";

export default function PreptestRouter() {
  return (
    <Route element={<RootPage />} path={NavigationRoutes.preptest.root}>
      <Route path="" element={<ContentPage />} />
      <Route
        path={NavigationRoutes.preptest.exams}
        element={
          <AuthGuard>
            <TestPage />
          </AuthGuard>
        }
      />
      <Route
        path={NavigationRoutes.preptest.results}
        element={<TestResutlsPage />}
      />
      <Route
        path={NavigationRoutes.preptest.guide}
        element={<PreptestGuidePage />}
      />
    </Route>
  );
}
