import React from "react";
import { Route } from "react-router-dom";
import { ContentPage, QuestionsPage, RootPage } from "../pages/questions/views";
import NavigationRoutes from "../routes/NavigationRoutes";

export default function QuestionsRouter() {
  return (
    <Route path={NavigationRoutes.questions.root} element={<RootPage />}>
      <Route path="" element={<ContentPage />} />
      <Route
        path={NavigationRoutes.questions.myquestions}
        element={<QuestionsPage />}
      />
    </Route>
  );
}
