import React from "react";
import HomeRouter from "./HomeRouter";
import { BrowserRouter } from "react-router-dom";
import AuthRouter from "./AuthRouter";
export default function Router() {
  return (
    <BrowserRouter>
      <HomeRouter />
      <AuthRouter />
    </BrowserRouter>
  );
}
