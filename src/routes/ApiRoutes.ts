/* eslint-disable import/no-anonymous-default-export */

export default {
  auth: {
    login: "auth/login",
    register: {
      email: "auth/register/email",
      phone: "auth/register/phone",
    },
    logout: (id: string) => `auth/logout/${id}`,
    resetPassword: "auth/reset",
    resendOtp: (id: string) => `auth/otp/resend/${id}`,
    verifyOtp: (id: string) => `auth/verify/${id}`,
    changePassword: "auth/password/change",
    userInfo: (id: string) => `auth/user/${id}`,
  },
  profile: {
    crud: "profile",
    user: (id: string) => `profile/user/${id}`,
    updatePicture: "profile/picture",
    all: "profile/all",
  },
  category: {
    educationalLevel: "category/educational/level",
    questionsCategories: "category/questions/categories",
    addQuestionsCategories: "category/questions",
    addUsersCategories: "category/users/add",
    getUsersCategories: "category/users",
    challengeCategory: (id?: string) =>
      id ? `category/challenge/${id}` : "category/challenge",
    get: "category",
    updateQuestionCategory: (categoryId: string) =>
      `category/questions/${categoryId}`,
    deleteQuestionCategory: (categoryId: string) =>
      `category/questions/${categoryId}`,
    updateEducationalLevel: (levelId: string) =>
      `category/educational/level/${levelId}`,
    deleteEducationalLevel: (levelId: string) =>
      `category/educational/level/${levelId}`,
    deleteUsersCategory: (categoryId: string) =>
      `category/users/delete/${categoryId}`,
    updateUsersCategory: (categoryId: string) =>
      `category/users/update/${categoryId}`,
  },

  feeds: {
    crud: "feed",
    userFeeds: "feed/user",
  },
  course: {
    crud: "course",
  },
  subject: {
    crud: "subject",
    delete: (subjectId: string) => `subject/${subjectId}`,
    update: (subjectId: string) => `subject/${subjectId}`,
    addMany: "subject/many",
  },
  billingRate: {
    crud: "billing-rate",
    update_delete: (rateId: string) => `billing-rate/${rateId}`,
  },
  pricing: {
    crud: (pricingId?: string) =>
      pricingId ? `pricing/${pricingId}` : "pricing",
  },
  user: {
    getUserByUsername: (username: string) => `user/${username}`,
  },
  competition: {
    crud: (id?: string) => (id ? `competition/${id}` : "competition"),
  },

  group: {
    crud: (path?: any) => (path ? `group/${path}` : "group"),
  },
  contest: {
    crud: (path?: string) => (path ? `contest/${path}` : "contest"),
  },
  home: {
    students: "students",
    teachers: "teachers",
  },
};
