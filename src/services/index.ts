import { v4 } from "uuid";
import { EducationalInfoModel, WorkExperienceInfoModel } from "../models";

export function generateId() {
  return v4();
}

export function getDataUrl(file: any) {
  const fileReader = new FileReader();
  let results = null;
  fileReader.readAsDataURL(file);
  fileReader.addEventListener("load", (e) => {
    results = e.target?.result;
  });
  return results;
}

export function ValidateSchoolInfo(info: EducationalInfoModel) {
  if (info.schoolName.length <= 8) {
    throw "School Name Too Short";
  }

  if (info.program.length <= 0) {
    throw "Enter Program of Study";
  }
  if (info.startDate.length <= 0) {
    throw "Start Date Required";
  }
}

export function ValidateWorkExperienceInfo(info: WorkExperienceInfoModel) {
  if (info.company.length <= 0) {
    throw "Enter Compay/Organization Name";
  }
  if (info.jobTitle.length <= 0) {
    throw "Enter Job Title";
  }
  if (info.role.length <= 0) {
    throw "Enter Role";
  }
  if (info.startDate.length <= 0) {
    throw "Enter Start Date of the Role";
  }
}
