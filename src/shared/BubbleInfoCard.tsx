import { IconButton, Stack, Typography } from "@mui/material";
import React from "react";
import { IoIosClose } from "react-icons/io";
import { RowContainer } from "../components";

interface IProps {
  title: string;
  handleRemove?: () => void;
}
export default function BubbleInfoCard({ title, handleRemove }: IProps) {
  return (
    <Stack
      boxShadow={(theme) =>
        `1px 1px 1px ${theme.palette.action.disabledBackground}`
      }
      width="100%"
      direction="row"
      alignItems="center"
      justifyContent="space-between"
      bgcolor={(theme) => theme.palette.background.paper}
      borderRadius="30px"
      paddingLeft={1.5}
    >
      <Typography style={{ flex: 1, textAlign: "left" }} variant="caption">
        {title}
      </Typography>
      {handleRemove && (
        <IconButton onClick={handleRemove} size="small">
          <IoIosClose />
        </IconButton>
      )}
    </Stack>
  );
}
