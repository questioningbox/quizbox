import { Typography } from "@mui/material";
import React, { useState } from "react";
import {
  CountryDropdown,
  RegionDropdown,
  CountryRegionData,
} from "react-country-region-selector";

interface IProps {
  handleChange?: (country: any) => void;
  label?: string;
}

export default function CustomCountryPicker({ handleChange, label }: IProps) {
  const [country, setCountry] = useState<string>("");
  return (
    <div>
      <Typography variant="body2">{label ? label : "Country"}</Typography>
      <CountryDropdown
        classes="custom-country-picker"
        value={country}
        onChange={(val) => {
          setCountry(val);
          handleChange && handleChange(val);
        }}
      />
    </div>
  );
}
