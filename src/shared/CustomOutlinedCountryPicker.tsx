import * as React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import { countries, CountryType } from "../pages/data/country.data";
import { Stack, Typography } from "@mui/material";
import { ChangeEvent } from "react";

interface IProps {
  label?: string;
  handleChange?: (e: CountryType) => void;
}
export default function CustomOutlinedCountryPicker({
  label,
  handleChange,
}: IProps) {
  return (
    <Stack spacing={0.5}>
      {label && <Typography variant="body1">{label}</Typography>}
      <Autocomplete
        id="country-select-demo"
        sx={{ width: "100%" }}
        options={countries}
        autoHighlight
        onChange={(e, m) => (handleChange && m ? handleChange(m) : {})}
        getOptionLabel={(option) => option.label}
        renderOption={(props, option) => (
          <Box
            component="li"
            sx={{
              "& > img": { mr: 2, flexShrink: 0 },
              overflow: "hidden",
            }}
            {...props}
          >
            <img
              loading="lazy"
              width="20"
              src={`https://flagcdn.com/w20/${option.code.toLowerCase()}.png`}
              srcSet={`https://flagcdn.com/w40/${option.code.toLowerCase()}.png 2x`}
              alt=""
            />
            {option.label} ({option.code}) +{option.phone}
          </Box>
        )}
        renderInput={(params) => (
          <TextField
            {...params}
            variant="outlined"
            fullWidth
            size="small"
            inputProps={{
              ...params.inputProps,
              autoComplete: "country", // disable autocomplete and autofill
            }}
          />
        )}
      />
    </Stack>
  );
}
