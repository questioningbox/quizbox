import { Pagination, Stack, TablePagination } from "@mui/material";
import React from "react";

interface IProps {
  page: number;
  totalPages: number;
  totalDocuments: number;
  pageSize: number;
  title?: string;
  handlePageChange?: (page: number) => void;
  handlePageSize?: (pageSize: number) => void;
}
export default function CustomPaginationView({
  page,
  pageSize,
  totalPages,
  totalDocuments,
  title,
  handlePageChange,
  handlePageSize,
}: IProps) {
  return (
    <Stack
      width="100%"
      alignItems="center"
      justifyContent="center"
      border={(theme) => `1px solid ${theme.palette.action.disabledBackground}`}
      spacing={2}
      padding={0.5}
      borderRadius={0.85}
    >
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={totalDocuments}
        rowsPerPage={pageSize}
        page={page}
        onPageChange={(e, page) =>
          handlePageChange ? handlePageChange(page) : {}
        }
        onRowsPerPageChange={(e) =>
          handlePageSize ? handlePageSize(parseInt(e.target.value)) : {}
        }
      />
      {/* <Pagination
        page={page}
        count={totalPages}
        title={title ? title : "Record"}
        shape="rounded"
      /> */}
    </Stack>
  );
}
