import * as React from "react";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { StaticTimePicker } from "@mui/x-date-pickers/StaticTimePicker";
import dayjs, { Dayjs } from "dayjs";
import { Stack } from "@chakra-ui/react";

export default function StaticCustomTimePicker() {
  const [value, setValue] = React.useState<Dayjs | null>(dayjs("hh:mm:s a"));
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <StaticTimePicker
        renderInput={() => <div />}
        value={value}
        onChange={(e) => setValue(e)}
        orientation="landscape"
      />
    </LocalizationProvider>
  );
}
