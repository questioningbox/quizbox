import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Slide from "@mui/material/Slide";
import { TransitionProps } from "@mui/material/transitions";
import { Checkbox, IconButton, Stack, Typography } from "@mui/material";
import { CustomDatePicker, CustomIconButton, InputGroup } from "../components";
import { MdOutlineBookmarkAdd } from "react-icons/md";
import { IoClose } from "react-icons/io5";
import { EducationalInfoModel } from "../models";
import { generateId, ValidateSchoolInfo } from "../services";
import { useAppDispatch } from "../app/hooks";
import { responseFailed } from "../features/slice/ResponseSlice";

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

interface IProps {
  open: boolean;
  handleClose: () => void;
  handleAddSchool: (info: EducationalInfoModel) => void;
  school: EducationalInfoModel | null;
}

export default function EducationInfoModal({
  open,
  handleClose,
  handleAddSchool,
  school,
}: IProps) {
  const [current, setCurrent] = React.useState<boolean>(false);
  const dispatch = useAppDispatch();
  const [info, setInfo] = React.useState<EducationalInfoModel>({
    id: generateId(),
    schoolName: "",
    startDate: "",
    endDate: "",
    program: "",
  });

  function handleAdd() {
    try {
      ValidateSchoolInfo(info);
      handleAddSchool({ ...info, id: school ? school.id : generateId() });
      setInfo({
        schoolName: "",
        startDate: "",
        endDate: "",
        program: "",
        id: generateId(),
      });
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  React.useEffect(() => {
    if (school) {
      setInfo({ ...school });
    }
  }, [info]);

  React.useEffect(() => {
    if (school) {
      setInfo({ ...school });
    }
  }, []);

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      PaperComponent={Stack}
      aria-describedby="alert-dialog-slide-description"
      fullWidth
      maxWidth="sm"
    >
      <DialogTitle
        sx={(theme) => ({
          bgcolor: theme.palette.common.white,
        })}
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          width="100%"
        >
          <Typography variant="body1">Eductional Info</Typography>
          <IconButton color="error" onClick={handleClose} size="small">
            <IoClose />
          </IconButton>
        </Stack>
      </DialogTitle>
      <DialogContent
        dividers
        sx={(theme) => ({
          bgcolor: theme.palette.common.white,
        })}
      >
        <Stack spacing={1.5}>
          <InputGroup
            handleChange={(e) =>
              setInfo({ ...info, schoolName: e.target.value })
            }
            placeholder="enter school name"
            label="School Name*"
            props={{
              value: info.schoolName,
            }}
          />
          <InputGroup
            handleChange={(e) => setInfo({ ...info, program: e.target.value })}
            placeholder="enter program of study"
            label="Program*"
            props={{
              value: info.program,
            }}
          />
          <CustomDatePicker
            handleChange={(val) =>
              setInfo({
                ...info,
                startDate: val,
              })
            }
            placeholder="Start Date"
          />
          <Stack spacing={1} direction="row" width="100%">
            <CustomDatePicker
              handleChange={(val) =>
                setInfo({
                  ...info,
                  endDate: val,
                })
              }
              placeholder="End Date"
            />
            <Stack direction="row" alignItems="center" justifyContent="center">
              <Typography variant="body2">Currently Here</Typography>
              <Checkbox
                value={current}
                onChange={() => {
                  setCurrent(!current);
                  setInfo({ ...info, endDate: "" });
                }}
              />
            </Stack>
          </Stack>
        </Stack>
      </DialogContent>
      <DialogActions
        sx={(theme) => ({
          bgcolor: theme.palette.common.white,
        })}
      >
        <CustomIconButton
          handleClick={handleAdd}
          title={school ? "Update" : "Add"}
          Icon={MdOutlineBookmarkAdd}
        />
      </DialogActions>
    </Dialog>
  );
}
