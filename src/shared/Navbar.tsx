import {
  AppBar,
  IconButton,
  Stack,
  TextField,
  Toolbar,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React from "react";
import { CiBellOn } from "react-icons/ci";
import { FiSearch } from "react-icons/fi";
import { IoMenuOutline } from "react-icons/io5";
import { VscFeedback } from "react-icons/vsc";
import { SearchInput } from "../components";
interface IProps {
  sidebar: boolean;
  handleSidebar: () => void;
}
export default function Navbar({ sidebar, handleSidebar }: IProps) {
  return (
    <AppBar
      sx={(theme) => ({
        color: theme.palette.common.black,
        backgroundColor: theme.palette.common.white,
        paddingLeft: sidebar ? "70px" : 0,
        height: "60px",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      })}
      position="fixed"
      elevation={0}
    >
      <Toolbar
        sx={(theme) => ({
          width: "100%",
          height: "100%",
          display: "flex",
          flexDirection: "column",
          alignItems: "centerr",
          justifyContent: "center",
        })}
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          width="100%"
          height="100%"
          paddingX={4}
        >
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="flex-start"
          >
            <Typography
              fontSize={(theme) => theme.spacing(2.5)}
              variant="body1"
              fontWeight={900}
            >
              QuizBox
            </Typography>
          </Stack>
          <Stack
            spacing={1}
            direction="row"
            alignItems="center"
            justifyContent="flex-end"
          >
            {useMediaQuery("(max-width:567px)") ? (
              <IconButton>
                <FiSearch />
              </IconButton>
            ) : (
              <SearchInput />
            )}
            <IconButton
              sx={(theme) => ({
                borderRadius: theme.spacing(1),
                borderColor: theme.palette.action.disabledBackground,
                borderWidth: 1,
                border: `1px solid ${theme.palette.action.hover}`,
              })}
              size="medium"
            >
              <CiBellOn />
            </IconButton>
            <IconButton onClick={handleSidebar} size="medium">
              <IoMenuOutline />
            </IconButton>
          </Stack>
        </Stack>
      </Toolbar>
    </AppBar>
  );
}
