import {
  Button,
  Grid,
  IconButton,
  MenuItem,
  Stack,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { BsListUl } from "react-icons/bs";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { CourseThunk } from "../functions/slice";
import ApiRoutes from "../routes/ApiRoutes";
import { getStoreItems } from "../utilities";
import { AppFooter, CourseInfoCard, StoreItem } from "../views";
import { PreptestMenu } from "./components";

export default function PrepTestPage() {
  const isMobile = useMediaQuery("(max-width:567px)");
  const [menu, setMenu] = useState<HTMLElement | null>(null);
  const { courses } = useAppSelector((state) => state.CoursesReducer);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(CourseThunk({ method: "get", url: ApiRoutes.course.crud }));
  }, []);
  return (
    <Stack
      sx={(theme) => ({
        overflowY: "auto",
      })}
      width="100%"
      padding={(theme) => theme.spacing(0.5, 4)}
    >
      <PreptestMenu anchorEl={menu} handleClose={() => setMenu(null)} />
      <Stack
        width="100%"
        padding={2}
        alignItems="center"
        justifyContent="center"
      >
        <Typography textAlign="center" variant="h5">
          Trial Prep Test: Lets Do This
        </Typography>
        <Typography textAlign="center" variant="caption">
          Level up your skills by taking this test and become ready for your
          upcoming exams
        </Typography>
        <TextField
          sx={(theme) => ({
            marginTop: theme.spacing(1),
          })}
          variant="outlined"
          size="small"
          placeholder="search...."
          InputProps={{
            className: "small-input",
          }}
        />
      </Stack>
      <Stack
        width="100%"
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        bgcolor={(theme) => theme.palette.action.hover}
        padding={1.5}
      >
        {!isMobile && (
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="flex-start"
          >
            {[
              "View All",
              "Design",
              "Product",
              "Software Engineering",
              "Customer Services",
            ].map((link) => (
              <Button
                variant="text"
                sx={(theme) => ({
                  textTransform: "none",
                })}
                size="small"
                color="inherit"
              >
                {link}
              </Button>
            ))}
          </Stack>
        )}
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          spacing={1}
          sx={(theme) => ({
            [theme.breakpoints.down("sm")]: {
              width: "100%",
            },
          })}
        >
          <TextField
            sx={(theme) => ({
              minWidth: "200px",
            })}
            select
            variant="outlined"
            size="small"
            defaultValue="Most Recent"
            InputProps={{
              className: "small-input",
            }}
          >
            <MenuItem value="Most Recent">Most recent</MenuItem>
          </TextField>
          {isMobile && (
            <IconButton onClick={(e) => setMenu(e.currentTarget)} size="small">
              <BsListUl />
            </IconButton>
          )}
        </Stack>
      </Stack>
      <Stack marginY={1}>
        <Grid container spacing={1}>
          {courses.results.map((item) => (
            <CourseInfoCard
              parentProps={{ borderRadius: (theme) => theme.spacing(0.5) }}
              info={item}
            />
          ))}
        </Grid>
      </Stack>
      <AppFooter />
    </Stack>
  );
}
