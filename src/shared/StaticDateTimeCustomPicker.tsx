import * as React from "react";
import dayjs, { Dayjs } from "dayjs";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { MobileDateTimePicker } from "@mui/x-date-pickers/MobileDateTimePicker";
import { DesktopDateTimePicker } from "@mui/x-date-pickers/DesktopDateTimePicker";
import { StaticDateTimePicker } from "@mui/x-date-pickers/StaticDateTimePicker";
import { TextField } from "@mui/material";

interface IProps {
  handleChange: (value: any) => void;
}
export default function ResponsiveDateTimePickers({ handleChange }: IProps) {
  const [value, setValue] = React.useState<Dayjs | null>(
    dayjs("2022-04-17T15:30")
  );
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <StaticDateTimePicker
        onChange={(newValue) => {
          setValue(newValue);
          handleChange(newValue);
        }}
        renderInput={(params) => (
          <TextField fullWidth size="small" variant="outlined" {...params} />
        )}
        value={value}
      />
    </LocalizationProvider>
  );
}
