import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Slide from "@mui/material/Slide";
import { TransitionProps } from "@mui/material/transitions";
import { Checkbox, IconButton, Stack, Typography } from "@mui/material";
import { CustomDatePicker, CustomIconButton, InputGroup } from "../components";
import { MdOutlineBookmarkAdd } from "react-icons/md";
import { IoClose } from "react-icons/io5";
import { EducationalInfoModel, WorkExperienceInfoModel } from "../models";
import { generateId, ValidateWorkExperienceInfo } from "../services";
import { useAppDispatch } from "../app/hooks";
import { responseFailed } from "../features/slice/ResponseSlice";

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

interface IProps {
  open: boolean;
  handleClose: () => void;
  handleAddWork: (info: WorkExperienceInfoModel) => void;
  work: WorkExperienceInfoModel | null;
}

export default function EducationInfoModal({
  open,
  handleClose,
  handleAddWork,
  work,
}: IProps) {
  const [current, setCurrent] = React.useState<boolean>(false);
  const dispatch = useAppDispatch();
  const [info, setInfo] = React.useState<WorkExperienceInfoModel>({
    id: generateId(),
    company: "",
    startDate: "",
    endDate: "",
    role: "",
    jobTitle: "",
  });

  function handleAdd() {
    try {
      ValidateWorkExperienceInfo(info);
      handleAddWork({ ...info, id: work ? work.id : generateId() });
      setInfo({
        id: generateId(),
        company: "",
        startDate: "",
        endDate: "",
        role: "",
        jobTitle: "",
      });
    } catch (error) {
      dispatch(responseFailed(error));
    }
  }

  React.useEffect(() => {
    if (work) {
      setInfo({ ...work });
    }
  }, [work]);

  React.useEffect(() => {
    if (work) {
      setInfo({ ...work });
    }
  }, []);

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      PaperComponent={Stack}
      aria-describedby="alert-dialog-slide-description"
      fullWidth
      maxWidth="sm"
    >
      <DialogTitle
        sx={(theme) => ({
          bgcolor: theme.palette.common.white,
        })}
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          width="100%"
        >
          <Typography variant="body1">Work Experience</Typography>
          <IconButton color="error" onClick={handleClose} size="small">
            <IoClose />
          </IconButton>
        </Stack>
      </DialogTitle>
      <DialogContent
        dividers
        sx={(theme) => ({
          bgcolor: theme.palette.common.white,
        })}
      >
        <Stack spacing={1.5}>
          <InputGroup
            handleChange={(e) => setInfo({ ...info, company: e.target.value })}
            placeholder="enter company/organization name"
            label="Company/Organization Name*"
            props={{
              value: info.company,
            }}
          />
          <InputGroup
            handleChange={(e) => setInfo({ ...info, jobTitle: e.target.value })}
            placeholder="enter job title"
            label="Job Title*"
            props={{
              value: info.jobTitle,
            }}
          />
          <InputGroup
            handleChange={(e) => setInfo({ ...info, role: e.target.value })}
            placeholder="enter role or position"
            label="Role/Position*"
            props={{
              value: info.role,
            }}
          />
          <CustomDatePicker
            handleChange={(val) =>
              setInfo({
                ...info,
                startDate: val,
              })
            }
            placeholder="Start Date"
          />
          <Stack spacing={1} direction="row" width="100%">
            <CustomDatePicker
              handleChange={(val) =>
                setInfo({
                  ...info,
                  endDate: val,
                })
              }
              placeholder="End Date"
            />
            <Stack direction="row" alignItems="center" justifyContent="center">
              <Typography variant="body2">Currently Here</Typography>
              <Checkbox
                value={current}
                onChange={() => {
                  setCurrent(!current);
                  setInfo({ ...info, endDate: "" });
                }}
              />
            </Stack>
          </Stack>
        </Stack>
      </DialogContent>
      <DialogActions
        sx={(theme) => ({
          bgcolor: theme.palette.common.white,
        })}
      >
        <CustomIconButton
          handleClick={handleAdd}
          title={work ? "Update" : "Add"}
          Icon={MdOutlineBookmarkAdd}
        />
      </DialogActions>
    </Dialog>
  );
}
