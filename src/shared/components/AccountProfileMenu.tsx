import { Menu, MenuItem } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { userLogout } from "../../features/auth/UserSlice";
import { LogoutThunk } from "../../functions/auth";
import ApiRoutes from "../../routes/ApiRoutes";
import NavigationRoutes from "../../routes/NavigationRoutes";

interface IProps {
  handleClose: () => void;
  anchorEl: HTMLElement | null;
}
export default function AccountProfileMenu({ handleClose, anchorEl }: IProps) {
  const { user } = useAppSelector((state) => state.UserReducer);
  const dispatch = useAppDispatch();
  const navigation = useNavigate();
  return (
    <Menu
      anchorOrigin={{ vertical: "center", horizontal: "right" }}
      anchorEl={anchorEl}
      open={Boolean(anchorEl)}
      onClose={handleClose}
    >
      {user && (
        <React.Fragment>
          <MenuItem
            onClick={() => {
              handleClose();
              navigation(NavigationRoutes.student.profile);
            }}
            color="primary"
          >
            Dashboard
          </MenuItem>
          <MenuItem
            onClick={() => {
              handleClose();
              dispatch(
                LogoutThunk({
                  method: "put",
                  url: ApiRoutes.auth.logout(user.userId),
                  token: user.token,
                  data: null,
                })
              );
            }}
            sx={(theme) => ({
              color: theme.palette.error.main,
            })}
          >
            Logout
          </MenuItem>
        </React.Fragment>
      )}
      {!user && (
        <MenuItem
          onClick={() => {
            handleClose();
            navigation(NavigationRoutes.account.login);
          }}
        >
          Sign In
        </MenuItem>
      )}
    </Menu>
  );
}
