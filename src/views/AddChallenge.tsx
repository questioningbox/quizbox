import React, { useEffect, useState } from "react";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Slide from "@mui/material/Slide";
import { TransitionProps } from "@mui/material/transitions";
import { BubbleInfoCard, CustomIconButton, DialogHeader } from "../shared";
import {
  CustomDatePicker,
  CustomTimePicker,
  InputGroup,
  RowContainer,
  SmallInput,
} from "../components";
import {
  Divider,
  IconButton,
  MenuItem,
  Stack,
  Typography,
} from "@mui/material";
import { IoPersonAddOutline } from "react-icons/io5";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import FeaturesThunk from "../functions/slice/FeaturesThunk";
import ApiRoutes from "../routes/ApiRoutes";
import { RiSendPlaneLine } from "react-icons/ri";
import CreateChallengeDto from "../dto/CreateChallengeDto";
import UserModel, { UserInfo } from "../models/UserModel";
import { ErrorResponse } from "@remix-run/router";
import {
  responseFailed,
  responsePending,
  responseSuccessful,
} from "../features/slice/ResponseSlice";
import controller from "../controller";
import ApiResponseModel from "../models/ApiResponseModel";
import { IoIosClose } from "react-icons/io";

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

interface IProps {
  handleClose: () => void;
  open: boolean;
}

export default function AlertDialogSlide({ handleClose, open }: IProps) {
  const { features } = useAppSelector((state) => state.FeaturesReducer);
  const { user } = useAppSelector((state) => state.UserReducer);
  const [competitors, setCompetitors] = useState<UserModel[]>([]);
  const { loading } = useAppSelector((state) => state.ResponseReducer);
  const [username, setUsername] = useState("");
  const [info, setInfo] = useState<CreateChallengeDto>({
    title: "",
    tag: "",
    description: "",
    competitors: [],
    category: null,
    startDate: "",
    startTime: "",
  });
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(
      FeaturesThunk({
        method: "get",
        url: ApiRoutes.category.get,
        token: user?.token,
      })
    );
  }, []);

  async function GetChallengeCompetitor() {
    try {
      dispatch(responsePending());
      const res = await controller<UserModel>({
        method: "get",
        url: ApiRoutes.user.getUserByUsername(username),
        token: user?.token,
      });
      dispatch(responseSuccessful(""));
      setCompetitors([...competitors, res]);
      setInfo({ ...info, competitors: [...info.competitors, res] });
    } catch (error: any) {
      dispatch(responseFailed(error?.message || error));
    }
  }

  async function handleAddChallenge() {
    try {
      dispatch(responsePending());
      const d = await controller<ApiResponseModel<any>>({
        data: info,
        method: "post",
        token: user?.token,
        url: ApiRoutes.competition.crud(),
      });
      dispatch(responseSuccessful(d.message));
    } catch (error: any) {
      dispatch(responseFailed(error?.message || error));
    }
  }

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      aria-describedby="alert-dialog-slide-description"
      maxWidth="md"
      fullWidth
    >
      <DialogTitle>
        <DialogHeader title="Add Challenge" handleClose={handleClose} />
      </DialogTitle>
      <DialogContent dividers>
        <Stack
          width="100$"
          direction="row"
          sx={(theme) => ({
            [theme.breakpoints.down("sm")]: {
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "flex-start",
            },
          })}
          spacing={0}
        >
          <Stack
            sx={(theme) => ({
              [theme.breakpoints.down("sm")]: {
                width: "100%",
              },
            })}
            spacing={1}
            flex={1}
          >
            <InputGroup
              label="Challenge Title*"
              placeholder="enter title of challenge"
              handleChange={(e) => setInfo({ ...info, title: e.target.value })}
              props={{ value: info.title }}
            />
            <InputGroup
              label="Description*"
              placeholder="challenge description"
              handleChange={(e) =>
                setInfo({ ...info, description: e.target.value })
              }
              props={{ value: info.description, multiline: true }}
            />
            <InputGroup
              label="Challenge Category*"
              props={{ select: true }}
              placeholder="category"
            >
              {features.challengeCategories.map((cate) => (
                <MenuItem
                  onClick={() => setInfo({ ...info, category: cate })}
                  key={cate.categoryId}
                  value={cate.categoryId}
                >
                  {cate.title}
                </MenuItem>
              ))}
            </InputGroup>
            <InputGroup
              handleChange={(e) => setInfo({ ...info, tag: e.target.value })}
              label="Tag"
              placeholder="#tag"
            />
          </Stack>
          <Stack
            sx={(theme) => ({
              [theme.breakpoints.down("sm")]: {
                width: "100%",
                padding: theme.spacing(1, 0),
                alignItems: "center",
                justifyContent: "center",
                margin: 0,
              },
            })}
            flex={1}
            spacing={1}
            padding={2.5}
          >
            <CustomDatePicker
              handleChange={(val) => setInfo({ ...info, startDate: val })}
              placeholder="Challenge Date"
            />
            <CustomTimePicker
              label="Challenge Time"
              handleChange={(value) => setInfo({ ...info, startTime: value })}
            />
            <Typography variant="body1" color="default">
              Competitors
            </Typography>
            <RowContainer>
              <SmallInput
                handleChange={(e) => setUsername(e.target.value)}
                style={{ flex: 1 }}
                placeholder="Phone or Email"
              />
              <CustomIconButton
                title="Add"
                variant="contained"
                Icon={IoPersonAddOutline}
                size="small"
                props={{ disabled: loading }}
                handleClick={GetChallengeCompetitor}
              />
            </RowContainer>
            <Divider />
            {competitors.map((c) => (
              <BubbleInfoCard
                title={c?.name}
                handleRemove={() => {
                  setCompetitors(
                    competitors.filter((co) => co.userId !== c.userId)
                  );
                  setInfo({
                    ...info,
                    competitors: info.competitors.filter(
                      (cp) => cp.userId !== c.userId
                    ),
                  });
                }}
              />
            ))}
          </Stack>
        </Stack>
      </DialogContent>
      <DialogActions>
        <CustomIconButton
          variant="contained"
          title="Submit"
          Icon={RiSendPlaneLine}
          handleClick={handleAddChallenge}
          props={{ disabled: loading }}
        />
      </DialogActions>
    </Dialog>
  );
}
