import {
  Button,
  Divider,
  MenuItem,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import { SmallInput } from "../components";
import { FooterLinks } from "../data";
import { CustomIconButton } from "../shared";
import Footer from "./Footer";

export default function AppFooter() {
  const navigation = useNavigate();
  return (
    <Stack
      sx={(theme) => ({
        width: "100%",
        padding: theme.spacing(2),
        bgcolor: theme.palette.action.hover,
        [theme.breakpoints.down("sm")]: {
          paddingBottom: "20px",
        },
        paddingBottom: "10px",
      })}
      spacing={1}
    >
      <Stack
        direction="row"
        padding={(theme) => theme.spacing(0, 1)}
        alignItems="center"
        justifyContent="space-between"
        sx={(theme) => ({
          [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "flex-start",
          },
        })}
        spacing={1.5}
      >
        <Stack
          alignItems="center"
          sx={(theme) => ({
            [theme.breakpoints.down("sm")]: {
              width: "100%",
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            },
          })}
          justifyContent="flex-start"
        >
          <MenuItem
            sx={(theme) => ({
              padding: theme.spacing(0.5),
              marginRight: "5px",
              height: "35px",
            })}
            onClick={() => navigation("/")}
          >
            <Typography
              textAlign="left"
              width="100%"
              variant="h6"
              fontWeight={900}
            >
              QuizBox
            </Typography>
          </MenuItem>
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="flex-start"
            spacing={1}
            width="100%"
          >
            {FooterLinks.map((info) => {
              return (
                <Button
                  key={info.title}
                  variant="text"
                  size="small"
                  sx={(theme) => ({
                    textTransform: "none",
                    color: theme.palette.common.black,
                    fontSize: theme.spacing(1.5),
                    bgcolor: "transparent",
                  })}
                  onClick={() => navigation(info.route)}
                >
                  {info.title}
                </Button>
              );
            })}
          </Stack>
        </Stack>
        <Stack
          sx={(theme) => ({
            [theme.breakpoints.down("sm")]: {
              width: "100%",
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-start",
              padding: theme.spacing(1.5, 0),
            },
          })}
        >
          <Typography variant="body2">Subscribe for News & Updates</Typography>
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="flex-start"
            spacing={1}
            sx={(theme) => ({
              [theme.breakpoints.down("sm")]: {
                marginLeft: "5px",
              },
            })}
          >
            <SmallInput placeholder="Email/PhoneNumber" />
            <CustomIconButton title="Submit" size="small" variant="contained" />
          </Stack>
        </Stack>
      </Stack>
      <Divider />
      <Footer />
    </Stack>
  );
}
