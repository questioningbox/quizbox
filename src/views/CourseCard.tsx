import {
  Stack,
  Grid,
  Typography,
  Chip,
  StackProps,
  IconButton,
} from "@mui/material";
import dayjs from "dayjs";
import React from "react";
import { AiOutlineMore } from "react-icons/ai";
import { FiArrowUpRight } from "react-icons/fi";
import { useNavigate } from "react-router-dom";
import { AppColors } from "../constants";
import { CourseInfo, CourseModel } from "../models/CourseModel";
import resources from "../resources";
import NavigationRoutes from "../routes/NavigationRoutes";
import { CustomIconButton } from "../shared";
import { IStoreItem } from "../utilities";

interface IProps {
  parentProps?: StackProps;
  handleMore?: () => void;
  info: CourseInfo;
}
export default function CourseCard({ parentProps, handleMore, info }: IProps) {
  const navigation = useNavigate();
  return (
    <Grid md={5} sm={6} xs={12} lg={4} xl={3} item>
      <Stack
        width="100%"
        borderRadius={(theme) => theme.spacing(0.75)}
        overflow="hidden"
        border={(theme) =>
          `1px solid ${theme.palette.action.disabledBackground}`
        }
        {...parentProps}
      >
        <Stack position="relative" height="200px">
          <img
            className="img"
            alt="store-item-image"
            src={info.course.coverImage.secureUrl}
          />
          <Stack
            position="absolute"
            padding={1}
            zIndex={101}
            direction="row"
            alignItems="center"
            justifyContent="space-between"
            width="100%"
            sx={(theme) => ({
              bottom: 0,
              left: 0,
              width: "100%",
            })}
            className="glass-bg"
          >
            <Stack flex={1}>
              <Typography
                color={(theme) => theme.palette.common.white}
                variant="caption"
                fontWeight={900}
                sx={(theme) => ({
                  alignItems: "center",
                  justifyContent: "flex-start",
                  display: "flex",
                  flexDirection: "row",
                })}
              >
                {info.author.user?.name}{" "}
              </Typography>
              <Typography
                fontSize={(theme) => theme.spacing(1.25)}
                variant="caption"
                color={(theme) => theme.palette.common.white}
              >
                {dayjs(info.course.duration.startDate).format("DD MMMM YYYY")} -{" "}
                <small>
                  {`( ${info.course.duration.duration} ${info.course.duration.period} )`}
                </small>
              </Typography>
            </Stack>
            <Stack>
              <Typography
                fontSize={(theme) => theme.spacing(1.25)}
                color={(theme) => theme.palette.common.white}
                variant="body2"
                sx={(theme) => ({
                  textTransform: "capitalize",
                })}
              >
                {info.course.courseType}
              </Typography>
            </Stack>
          </Stack>
        </Stack>
        <Stack padding={1}>
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="space-between"
            width="100%"
          >
            <Typography fontWeight={700} variant="body2">
              {info.course.title}
            </Typography>
            {handleMore && (
              <IconButton size="small" onClick={handleMore}>
                <AiOutlineMore />
              </IconButton>
            )}
          </Stack>
          <Typography fontSize={(theme) => theme.spacing(1.45)} variant="body2">
            {info.course.description}
          </Typography>
        </Stack>
        <Stack
          padding={(theme) => theme.spacing(0.25, 1)}
          direction="row"
          alignItems="cente"
          justifyContent="flex-end"
          width="100%"
          paddingBottom={1}
        >
          <CustomIconButton
            size="xsmall"
            handleClick={() =>
              navigation(
                `${NavigationRoutes.home.course}?q=info&id=${info.course.courseId}`
              )
            }
            title="Readmore"
            variant="outlined"
          />
        </Stack>
      </Stack>
    </Grid>
  );
}
