import { Grow, Stack, Typography } from "@mui/material";
import React from "react";
import { avatarImages } from "../resources";

export default function DummyUsersContainer() {
  return (
    <Grow in style={{ transformOrigin: "0 0 0" }} timeout={1500}>
      <Stack
        direction="row"
        position="relative"
        width="100%"
        padding={(theme) => theme.spacing(1)}
        alignItems="center"
      >
        {[
          avatarImages.avatar1,
          avatarImages.avatar2,
          avatarImages.avatar3,
          avatarImages.avatar4,
          avatarImages.avatar5,
        ].map((img, i) => (
          <Stack
            key={img}
            marginLeft={(theme) => theme.spacing(-1.5)}
            zIndex={101}
            bgcolor="red"
            width={50}
            height={50}
            borderRadius="100%"
            overflow="hidden"
            sx={(theme) => ({
              [theme.breakpoints.down("sm")]: {
                width: 40,
                height: 40,
              },
            })}
          >
            <img alt={`avatar-${i}`} src={img} />
          </Stack>
        ))}
        <Stack marginX={(theme) => theme.spacing(1)} />
        <Typography variant="caption">Join over 40,000+ users</Typography>
      </Stack>
    </Grow>
  );
}
