import React, { useEffect, useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Slide from "@mui/material/Slide";
import { TransitionProps } from "@mui/material/transitions";
import FeedModel from "../models/FeedModel";
import {
  Divider,
  IconButton,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import {
  AiFillLike,
  AiOutlineComment,
  AiOutlineLike,
  AiOutlineSend,
} from "react-icons/ai";
import { MdClose } from "react-icons/md";
import { FeedIconButton, UserAvatar } from "../components";
import ProfileModel from "../models/ProfileModel";
import { useAppSelector } from "../app/hooks";
import controller, { baseUrl } from "../controller";
import UserModel from "../models/UserModel";
import SocketIo, { Socket } from "socket.io-client";
import { FeedCommentModel } from "../models";
import EventNames from "../routes/EventNames";
import dayjs from "dayjs";
import { generateId } from "../services";
import { FeedComment } from "../pages/home/components";

let socket: Socket;
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

interface IProps {
  open: boolean;
  handleClose: () => void;
  feed: FeedModel;
}
export default function FeedsCommentModal({ open, handleClose, feed }: IProps) {
  const [userInfo, setUserInfo] = useState<UserModel | null>(null);
  const [Feed, setFeed] = useState<FeedModel>(feed);
  const { feeds } = useAppSelector((state) => state.FeedsReducer);
  const { user } = useAppSelector((state) => state.UserReducer);
  const [profileInfo, setProfileInfo] = useState<ProfileModel | null>(null);
  const [comment, setComment] = useState<FeedCommentModel>({
    comment: "",
    userId: "",
    id: "",
    createdAt: "",
    likes: [],
  });
  async function getInfo() {
    const uInfo = await controller<UserModel>({
      method: "get",
      url: `feed/user/info/${feed.userId}`,
      token: "",
      data: null,
    });
    setUserInfo(uInfo);
  }

  async function getProfile() {
    const uInfo = await controller<ProfileModel>({
      method: "get",
      url: `feed/user/profile/${feed.userId}`,
      token: "",
      data: null,
    });
    setProfileInfo(uInfo);
  }

  function handleLike(info: FeedModel) {
    if (user) {
      if (info.likes.includes(user.userId)) {
        socket.emit(EventNames.feed.comment, {
          ...info,
          likes: info.likes.filter((like) => like !== user.userId),
        });
      } else {
        socket.emit(EventNames.feed.comment, {
          ...info,
          likes: [...info.likes, user?.userId],
        });
      }
    }
  }

  function handleComment() {
    socket.emit(EventNames.feed.comment, {
      ...Feed,
      comments: [
        ...Feed.comments,
        {
          ...comment,
          userId: user?.userId,
          createdAt: dayjs().format(),
          likes: [],
          id: generateId(),
        },
      ],
    });
    setComment({ comment: "", userId: "", id: "", createdAt: "", likes: [] });
  }

  useEffect(() => {
    socket = SocketIo(baseUrl);
    //

    getInfo();
    getProfile();
  }, []);

  useEffect(() => {
    const f = feeds.find((f) => f._id === feed._id);
    if (f) {
      setFeed(f);
    }
  }, [feeds]);

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      PaperComponent={Stack}
      maxWidth="sm"
      fullWidth
    >
      <DialogTitle
        sx={(theme) => ({
          bgcolor: theme.palette.common.white,
        })}
      >
        <Stack
          alignItems="center"
          justifyContent="center"
          direction="row"
          spacing={1.5}
          width="100%"
        >
          <Stack>
            <UserAvatar
              source={profileInfo ? profileInfo.profileImage.secureUrl : ""}
            />
          </Stack>
          <Stack flex={1}>
            <Typography variant="body1">{userInfo?.name}</Typography>
            <Divider />
          </Stack>
          <IconButton size="small" onClick={handleClose} color="error">
            <MdClose />
          </IconButton>
        </Stack>

        <Stack>
          <Stack paddingLeft={3.5}>
            <Typography variant="caption">{Feed.message}</Typography>
          </Stack>
          <Stack
            padding={(theme) => theme.spacing(0.25, 0)}
            alignItems="center"
            justifyContent="flex-start"
            direction="row"
            spacing={1}
            paddingLeft={3.5}
          >
            <Typography variant="caption">
              {dayjs(Feed.createdAt).format("dd, DD/MM/YYYY hh:mm a")}
            </Typography>
            <FeedIconButton
              handleClick={() => {}}
              label={Feed.likes.length.toString()}
              Icon={
                Feed.likes.includes(user ? user.userId : "")
                  ? AiFillLike
                  : AiOutlineLike
              }
            />
            <FeedIconButton
              handleClick={() => handleLike(Feed)}
              label={Feed.comments.length.toString()}
              Icon={AiOutlineComment}
            />
          </Stack>
        </Stack>
      </DialogTitle>
      <DialogContent
        dividers
        sx={(theme) => ({
          bgcolor: theme.palette.common.white,
        })}
      >
        <Typography variant="body2">Comments</Typography>
        <Stack spacing={2} padding={(theme) => theme.spacing(2)}>
          {Feed.comments.map((fc) => (
            <FeedComment key={fc.id} comment={fc} />
          ))}
        </Stack>
      </DialogContent>
      {user && (
        <DialogActions
          sx={(theme) => ({
            bgcolor: theme.palette.common.white,
          })}
        >
          <Stack width="100%" direction="row" spacing={1.5}>
            <TextField
              variant="outlined"
              size="small"
              multiline
              placeholder="comment..."
              sx={(theme) => ({
                flex: 1,
              })}
              value={comment.comment}
              onChange={(e) =>
                setComment({ ...comment, comment: e.target.value })
              }
            />
            <IconButton
              color="primary"
              size="small"
              sx={(theme) => ({
                width: "50px",
                borderRadius: theme.spacing(0.5),
                border: `1px solid ${theme.palette.action.disabledBackground}`,
              })}
              disabled={!Boolean(comment.comment.length)}
              onClick={handleComment}
            >
              <AiOutlineSend />
            </IconButton>
          </Stack>
        </DialogActions>
      )}
    </Dialog>
  );
}
