import {
  Divider,
  Drawer,
  IconButton,
  Stack,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useAppSelector } from "../app/hooks";
import FeedModel, { FeedDto } from "../models/FeedModel";
import EventNames from "../routes/EventNames";
import SocketClient, { Socket } from "socket.io-client";
import { baseUrl } from "../controller";
import { AiOutlineClose } from "react-icons/ai";
import { Feed } from "../pages/home/components";
import { EmptyFeedsCard } from "../components";
import { FiSend } from "react-icons/fi";
import FeedsCommentModal from "./FeedsCommentsModal";
interface IProps {
  open: boolean;
  handleClose: () => void;
}

let socket: Socket;
export default function FeedsDrawer({ open, handleClose }: IProps) {
  const { feeds } = useAppSelector((state) => state.FeedsReducer);
  const [feed, setFeed] = useState<FeedModel | null>(null);
  const { user } = useAppSelector((state) => state.UserReducer);
  const [feedInfo, setFeedInfo] = useState<FeedDto>({
    userId: "",
    message: "",
  });

  useEffect(() => {
    socket = SocketClient(baseUrl);
  }, []);

  function sendFeed() {
    socket.emit(EventNames.feed.add, {
      ...feedInfo,
      userId: user ? user.userId : "",
    });

    setFeedInfo({ ...feedInfo, message: "" });
  }

  function handleLike(info: FeedModel) {
    if (user) {
      if (info.likes.includes(user.userId)) {
        socket.emit(EventNames.feed.comment, {
          ...info,
          likes: info.likes.filter((like) => like !== user.userId),
        });
      } else {
        socket.emit(EventNames.feed.comment, {
          ...info,
          likes: [...info.likes, user?.userId],
        });
      }
    }
  }
  return (
    <Drawer
      variant="persistent"
      anchor="right"
      open={open}
      onClose={handleClose}
    >
      {feed && (
        <FeedsCommentModal
          open={Boolean(feed)}
          handleClose={() => setFeed(null)}
          feed={feed}
        />
      )}
      <Stack
        sx={(theme) => ({
          width: "480px",
          [theme.breakpoints.down("sm")]: {
            width: "100vw",
          },
        })}
        height="100vh"
      >
        <Stack
          padding={1}
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          position="sticky"
          zIndex={1002}
          bgcolor="inherit"
          top={0}
        >
          <Typography
            fontSize={(theme) => theme.spacing(2.5)}
            variant="body1"
            fontWeight={900}
          >
            Activity Feeds
          </Typography>
          <IconButton onClick={handleClose} color="error" size="small">
            <AiOutlineClose />
          </IconButton>
        </Stack>
        <Divider />

        {feeds && feeds?.length > 0 ? (
          <Stack
            height="100%"
            padding={(theme) => theme.spacing(1)}
            width="100%"
          >
            {feeds.map((feed) => (
              <Feed
                canComment
                handleLike={() => handleLike(feed)}
                handleComment={() => setFeed(feed)}
                info={feed}
              >
                <Typography
                  fontSize={(theme) => theme.spacing(1.25)}
                  variant="body2"
                >
                  {feed.message}
                </Typography>
              </Feed>
            ))}
          </Stack>
        ) : (
          <EmptyFeedsCard />
        )}
        <Divider />
        <Stack
          padding={1.5}
          border={(theme) => `1px solid ${theme.palette.action.hover}`}
          direction="row"
          width="100%"
          paddingBottom="20px"
          spacing={1}
        >
          <TextField
            fullWidth
            variant="outlined"
            multiline
            placeholder="Message.."
            size="small"
            value={feedInfo.message}
            onChange={(e) =>
              setFeedInfo({ ...feedInfo, message: e.target.value })
            }
          />

          <IconButton
            disabled={!Boolean(feedInfo.message.length)}
            sx={(theme) => ({
              borderRadius: theme.spacing(0.5),
              padding: theme.spacing(0.5),
              border: `1px solid ${theme.palette.action.disabledBackground}`,
              width: "40px",
            })}
            onClick={sendFeed}
            color="primary"
            size="small"
          >
            <FiSend />
          </IconButton>
        </Stack>
      </Stack>
    </Drawer>
  );
}
